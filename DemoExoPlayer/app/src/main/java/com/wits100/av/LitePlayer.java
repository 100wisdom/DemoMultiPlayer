package com.wits100.av;

import android.view.Surface;

/**
 * Created by chuanjiang.zh on 2016-08-11.
 */
public interface LitePlayer {

    /// 打开中
    public static final int EVENT_OPENING = 1;

    public static final int EVENT_OPENED = 2;

    /// 播放中
    public static final int EVENT_PLAYING = 3;
    /// 停止
    public static final int EVENT_STOPPED = 4;
    /// 流结束
    public static final int EVENT_END_OF_STREAM = 5;
    /// 错误
    public static final int EVENT_ERROR = 7;

    public static final int EVENT_BUFFER = 10;


    public static interface EventListener {

        void onPlayEvent(LitePlayer player, int event, long value);
    }



    public void setEventListener(EventListener listener);

    /**
     * 设置显示
     * @param surface
     */
    public void setDisplay(Surface surface);

    /**
     * 打开媒体源
     * @param url
     * @return
     */
    public boolean open(String url);

    public void close();

    /**
     * 判断是否已经打开
     * @return
     */
    public boolean isOpen();

    public void play();

    public void stop();

    /**
     * 抓图
     * @param filepath
     * @return
     */
    public boolean snap(String filepath);

    /**
     * 判断媒体格式是否就绪
     * @return
     */
    public boolean isFormatReady();

    public int getState();

}
