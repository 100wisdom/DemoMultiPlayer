package com.wits100.av;

/**
 * Created by chuanjiang.zh on 2016-08-07.
 */
public class MPacket {
    public int type;    ///@see MediaType
    public byte[] data;
    public int flags;
    public int duration;
    public long ts;

    public int length() {
        return (data == null) ? 0 : data.length;
    }
}
