package com.wits100.av;

/**
 * Created by chuanjiang.zh on 2016-08-07.
 */
public class MediaType {

    /// state

    public static final int STATE_STOPPED = 0;
    public static final int STATE_PAUSED = 1;
    public static final int STATE_PLAYING = 2;

    /// type
    public static final int MEDIA_TYPE_NONE = 0;
    public static final int MEDIA_TYPE_VIDEO = 1;
    public static final int MEDIA_TYPE_AUDIO = 2;
    public static final int MEDIA_TYPE_DATA = 3;
    public static final int TYPE_SUBTITLE = 4;

    public static final int AV_PKT_FLAG_KEY = 0x0001;

    /// codec
    public static final int MCODEC_NONE = 0;
    public static final int MCODEC_MJPEG = 8;
    public static final int MCODEC_H264 = 28;
    public static final int MCODEC_HEVC = 174; /// H.265
    public static final int MCODEC_H265 = MCODEC_HEVC;
    public static final int MCODEC_G711U = 65542;
    public static final int MCODEC_G711A = 65543;

    public static final int MCODEC_MP3 = 0x15001;
    public static final int MCODEC_AAC = 0x15002;
    public static final int MCODEC_AC3 = 0x15003;
    public static final int MCODEC_VORBIS = 0x15005;
    public static final int MCODEC_RAW = 0x10101010;


    public static int MKBETAG(char ch0, char ch1, char ch2, char ch3)
    {
        return (((int) ch0))
                | (((int) ch1) << 8)
                | (((int) ch2) << 16)
                | (((int) ch3) << 24);
    }


    public static final int MEDIA_EVENT_OPENING = 0x0100;	/// 打开文件中
    public static final int MEDIA_EVENT_OPEN_FAILED = 0x0100 + 1;		/// 打开失败
    public static final int MEDIA_EVENT_OPENED = 0x0100 + 2;			/// 打开成功
    public static final int MEDIA_EVENT_CLOSED = 0x0100 + 3;			/// 文件关闭

    public static final int MEDIA_EVENT_END = 0x0100 + 4;		/// 播放结束

    public static final int MEDIA_EVENT_FORMAT_READY = 0x0100 + 5;	/// 已知媒体格式

    public static final int MEDIA_EVENT_SEEKING = 0x0100 + 6;	/// 拖拽中
    public static final int MEDIA_EVENT_SEEKED = 0x0100 + 7; 	/// 拖拽完成

    public static final int MEDIA_EVENT_FULLSCREEN = 0x0100 + 8;	/// 全屏变化
    public static final int MEDIA_EVENT_VOLUME  = 0x0100 + 9; 		/// 音量变化
    public static final int MEDIA_EVENT_MUTE = 0x0100 + 10;		/// 静音变化
    public static final int MEDIA_EVENT_SCALE = 0x0100 + 11;		/// 播放速度变化

    public static final int MEDIA_EVENT_PLAYING = 0x0100 + 12;		/// 播放中
    public static final int MEDIA_EVENT_PAUSED = 0x0100 + 13;			/// 暂停
    public static final int MEDIA_EVENT_STOPPED = 0x0100 + 14;		/// 停止播放

    public static final int MEDIA_EVENT_PACKET = 273;

}
