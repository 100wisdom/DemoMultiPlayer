package com.wits100.av;

import android.view.Surface;


/**
 * Created by chuanjiang.zh on 2016-08-15.
 */
public interface VideoRender {


    boolean open(MFormat fmt, Surface surface);

    void close();

    boolean isOpen();

    void setDisplay(Surface surface);

    void input(MPacket pkt);


}
