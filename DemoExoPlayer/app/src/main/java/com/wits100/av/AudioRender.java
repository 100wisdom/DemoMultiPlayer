package com.wits100.av;

/**
 * Created by zhengboyuan on 2016-12-28.
 */

public interface AudioRender {

    boolean open(MFormat fmt);

    void close();

    boolean isOpen();

    void input(MPacket pkt);


}
