package com.wits100.av;

/**
 * Created by chuanjiang.zh on 2016-08-14.
 */
public interface JniMediaReaderListener {

    void onReaderEvent(int handle, int event, long value);

}
