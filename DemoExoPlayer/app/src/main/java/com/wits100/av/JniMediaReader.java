package com.wits100.av;

import android.util.Log;

/**
 * Created by chuanjiang.zh on 2016-08-12.
 */
public class JniMediaReader {

    static {
        try {
            Log.i("jni", "load library");

            System.loadLibrary("MediaReader");

            System.loadLibrary("avutil-55");
            System.loadLibrary("avcodec-57");
            System.loadLibrary("avformat-57");
            System.loadLibrary("swscale-4");
            System.loadLibrary("swresample-2");
            System.loadLibrary("avfilter-6");

            mreader_init();

        } catch (UnsatisfiedLinkError e) {
            Log.e("jni", "failed to load library." + e.toString());
        }
    }


    /**
     * 初始化
     *
     * @return
     */
    public native static int mreader_init();

    /**
     *
     */
    public native static void mreader_quit();

    /**
     * 打开媒体源
     *
     * @param handle 返回的句柄
     * @param url    媒体源URL
     * @param params 可选参数
     * @return 0 表示成功, 其他值表示错误码
     */
    public native static int mreader_open(int[] handle, String url, String params);

    /**
     * 关闭媒体源
     *
     * @param handle
     * @return
     */
    public native static int mreader_close(int handle);

    /**
     * 媒体源是否打开
     *
     * @param handle
     * @return > 0 表示已经打开
     */
    public native static int mreader_isOpen(int handle);

    /**
     * 获取媒体格式
     *
     * @param handle
     * @param fmt
     * @return
     */
    public native static int mreader_getFormat(int handle, MFormat fmt);

    /**
     * 获取媒体时长
     *
     * @param handle   媒体源句柄
     * @param duration 媒体时长, 单位为毫秒
     * @return
     */
    public native static int mreader_getDuration(int handle, int[] duration);

    /**
     * 是否为实时媒体源
     *
     * @param handle
     * @return > 0 表示为实时媒体源
     */
    public native static int mreader_isLive(int handle);

    /**
     * 是否可以定位
     *
     * @param handle
     * @return > 0 表示可定位
     */
    public native static int mreader_seekable(int handle);

    /**
     * 播放媒体源
     *
     * @param handle
     * @return
     */
    public native static int mreader_play(int handle);

    /**
     * 暂停媒体源
     *
     * @param handle
     * @return
     */
    public native static int mreader_pause(int handle);

    /**
     * 停止媒体源
     *
     * @param handle
     * @return
     */
    public native static int mreader_stop(int handle);

    /**
     * 获取媒体源状态
     *
     * @param handle
     * @return
     */
    public native static int mreader_getState(int handle);

    /**
     * 读取媒体包
     *
     * @param handle
     * @param pkt
     * @return
     */
    public native static int mreader_read(int handle, MPacket pkt);

    /**
     * 中断读取操作
     *
     * @param handle
     * @return
     */
    public native static int mreader_interrupt(int handle);


    /**
     * 定位到指定偏移, 单位为微秒
     *
     * @param handle
     * @param offset
     * @return
     */
    public native static int mreader_seek(int handle, long offset);

    /**
     * 获取当前时间戳, 单位为微秒
     *
     * @param handle
     * @param offset
     * @return
     */
    public native static int mreader_getTime(int handle, long[] offset);

    /**
     * 开始记录为文件
     *
     * @param handle
     * @param filename 录像文件
     * @return
     */
    public native static int mreader_startRecord(int handle, String filename);

    /**
     * 停止记录
     *
     * @param handle
     * @return
     */
    public native static int mreader_stopRecord(int handle);

    /**
     * 是否正在记录
     *
     * @param handle
     * @return > 0 表示正在记录
     */
    public native static int mreader_isRecording(int handle);


    public native static int mreader_setEventCallback(int handle, JniMediaReaderListener listener);


}
