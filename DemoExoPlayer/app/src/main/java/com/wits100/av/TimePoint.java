package com.wits100.av;

/**
 * Created by zhengboyuan on 2017-02-11.
 */

public class TimePoint {

    public long pts;
    public long clock;

    public TimePoint()
    {
        pts = 0;
        clock = 0;
    }

    public TimePoint(long pts, long clock)
    {
        this.pts = pts;
        this.clock = clock;
    }

    public void reset(long pts, long clock)
    {
        this.pts = pts;
        this.clock = clock;
    }

    public void reset() {
        this.pts = 0;
        this.clock = 0;
    }

    public void reset(TimePoint pt) {
        this.pts = pt.pts;
        this.clock = pt.clock;
    }


    public boolean isSet() {
        return (clock == 0) && (pts == 0);
    }




    public long absDelta(TimePoint fromPoint) {
        return this.pts - fromPoint.pts;
    }


    public long delta(TimePoint fromPoint)
    {
        return (pts - fromPoint.pts) - (clock - fromPoint.clock);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("pts: ");
        sb.append(pts);
        sb.append(",clock: ");
        sb.append(clock);
        return sb.toString();
    }


}
