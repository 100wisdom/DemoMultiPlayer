package com.wits100.av;

import android.view.Surface;

/**
 * Created by chuanjiang.zh on 2016-08-12.
 */
public class MediaReader {

    public static final int EPERM   =        1;
    public static final int ENOENT  =        2;
    public static final int ESRCH   =        3;
    public static final int EINTR   =        4;
    public static final int EIO     =        5;
    public static final int ENXIO   =        6;
    public static final int E2BIG   =        7;
    public static final int ENOEXEC =        8;
    public static final int EBADF   =        9;
    public static final int ECHILD  =        10;
    public static final int EAGAIN  =        11;
    public static final int ENOMEM  =        12;
    public static final int EACCES  =        13;
    public static final int EFAULT  =        14;
    public static final int EBUSY   =        16;
    public static final int EEXIST  =        17;
    public static final int EXDEV   =        18;
    public static final int ENODEV  =        19;
    public static final int ENOTDIR =        20;
    public static final int EISDIR  =        21;
    public static final int ENFILE  =        23;
    public static final int EMFILE  =          24;
    public static final int ENOTTY  =        25;
    public static final int EFBIG   =        27;
    public static final int ENOSPC  =        28;
    public static final int ESPIPE  =        29;
    public static final int EROFS   =        30;
    public static final int EMLINK  =        31;
    public static final int EPIPE   =        32;
    public static final int EDOM    =        33;
    public static final int EDEADLK =        36;
    public static final int ENAMETOOLONG  =  38;
    public static final int ENOLCK  =          39;
    public static final int ENOSYS  =          40;
    public static final int ENOTEMPTY =        41;

    public boolean open(String url, String params) {
        boolean done = false;
        int[] handle = new int[1];
        int ret = JniMediaReader.mreader_open(handle, url, params);
        if (ret == 0)
        {
            mHandle = handle[0];
            done = true;
        }
        return done;
    }

    public void close() {
        JniMediaReader.mreader_close(mHandle);
        mHandle = -1;
    }

    public boolean isOpen() {
        if (mHandle < 0) {
            return false;
        }

        int ret = JniMediaReader.mreader_isOpen(mHandle);
        return (ret > 0);
    }

    public boolean isReady() {
        return isOpen() && getFormat().isValid();
    }

    public MFormat getFormat() {
        MFormat fmt = new MFormat();
        int ret = JniMediaReader.mreader_getFormat(mHandle, fmt);
        return fmt;
    }

    public boolean play() {
        int ret = JniMediaReader.mreader_play(mHandle);
        return (ret == 0);
    }

    public void stop() {
        JniMediaReader.mreader_stop(mHandle);
    }

    public int read(MPacket pkt) {
        int ret = JniMediaReader.mreader_read(mHandle, pkt);
        return ret;
    }

    public void interrupt() {
        JniMediaReader.mreader_interrupt(mHandle);
    }

    public boolean startRecord(String filename) {
        int ret = JniMediaReader.mreader_startRecord(mHandle, filename);
        return (ret == 0);
    }

    public void stopRecord() {
        JniMediaReader.mreader_stopRecord(mHandle);
    }

    public boolean isRecording() {
        int ret = JniMediaReader.mreader_isRecording(mHandle);
        return ret > 0;
    }

    public void setEventListener(JniMediaReaderListener listener) {
        JniMediaReader.mreader_setEventCallback(mHandle, listener);
    }

    protected int   mHandle = -1;

}
