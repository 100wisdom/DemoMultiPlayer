package com.wits.demoexoplayer;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;

import com.wits100.av.ExoLitePlayer;
import com.wits100.av.JLitePlayer;
import com.wits100.av.LitePlayer;
import com.wits100.av.MediaType;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MainActivity extends AppCompatActivity {

    public static String TAG = "player";

    LitePlayer player;
    LitePlayer player2;
    LitePlayer player3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupPlayer();
    }

    @Override
    protected void onStop() {
        teardownPlayer();

        super.onStop();
    }

    public void onClickPlayStop(View view) {
        String url = "rtsp://192.168.10.55:554/";

        EditText editText = (EditText)findViewById(R.id.editTextUrl);
        url = editText.getText().toString();

        //url = "http://192.168.10.51:9080/media/test.mp4";

        if (player.getState() == MediaType.STATE_PLAYING) {
            stopPlay();
        } else {
            startPlay(url);
        }
    }

    private void setupPlayer() {
        player = new ExoLitePlayer(this);
        player = new JLitePlayer();
        player2 = new JLitePlayer();
        player3 = new JLitePlayer();

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        player.setDisplay(surfaceView.getHolder().getSurface());

        SurfaceView surfaceView2 = (SurfaceView) findViewById(R.id.surfaceView2);
        player2.setDisplay(surfaceView2.getHolder().getSurface());

        SurfaceView surfaceView3 = (SurfaceView) findViewById(R.id.surfaceView3);
        player3.setDisplay(surfaceView3.getHolder().getSurface());


        String url = "rtsp://192.168.10.51:554/";
        EditText editText = (EditText)findViewById(R.id.editTextUrl);
        editText.setText(url);
    }

    private void teardownPlayer() {
        player.close();
        player2.close();
        player3.close();
    }

    public void startPlay(String url) {
        if (false) {
            try {
                test();
            } catch (Throwable t) {
                t.printStackTrace();
            }
            return;
        }

        player.open(url);
        player.play();

        player2.open(url);
        player2.play();

        player3.open(url);
        player3.play();
    }

    public void stopPlay() {
        player.stop();
        player.close();

        player2.stop();
        player2.close();

        player3.stop();
        player3.close();
    }

    public void test() throws IOException {

        MediaCodec mMediaDecode = MediaCodec.createDecoderByType("audio/mp4a-latm");
        MediaFormat format = new MediaFormat();
        format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_SAMPLE_RATE, 16000);
        //format.setInteger(MediaFormat.KEY_BIT_RATE, 0);
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectMain);

        byte[] audioConfig = new byte[] { 0x0a, 0x10};
        ByteBuffer csd = ByteBuffer.wrap(audioConfig);
        format.setByteBuffer("csd-0", csd);

        mMediaDecode.configure(format, null, null, 0);
        mMediaDecode.start();

        ByteBuffer[] inputBuffers = mMediaDecode.getInputBuffers();
        int inputBufferIndex = mMediaDecode.dequeueInputBuffer(1000 * 100);
        if (inputBufferIndex >= 0)
        {
            ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
            inputBuffer.clear();
        }
    }
}
