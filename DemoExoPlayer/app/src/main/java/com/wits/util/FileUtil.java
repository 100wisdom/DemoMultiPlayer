package com.wits.util;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Created by chuanjiang.zh on 2016-08-10.
 */
public class FileUtil {

    public static void append(String fileName, String content) {
        FileWriter writer = null;
        try {
            // 打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件
            writer = new FileWriter(fileName, true);
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(writer != null){
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取目录大小,不做递归计算
     * @param path
     * @return
     */
    public static long getFolderSize(String path) {
        long fileSize = 0;
        File file = new File(path);
        File[] files = file.listFiles();
        for (File f: files) {
            if (f.isDirectory()) {
                fileSize += getFolderSize(f.getPath());
            } else {
                fileSize += f.length();
            }
        }
        return fileSize;
    }


}
