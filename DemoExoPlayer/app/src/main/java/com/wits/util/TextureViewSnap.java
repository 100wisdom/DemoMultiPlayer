package com.wits.util;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.TextureView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by zhengboyuan on 2016-12-29.
 */


public class TextureViewSnap {

    public static final String TAG = "util";


    public static boolean snap(TextureView textureView, String filename) {
        Bitmap bmp = textureView.getBitmap();
        return storeImage(bmp, filename);
    }

    private static boolean storeImage(Bitmap image, String filename) {
        File pictureFile = new File(filename);
        if (pictureFile == null) {
            return false;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            return true;
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return false;
    }


}
