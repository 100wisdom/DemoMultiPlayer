package com.wits.util;

import android.content.SharedPreferences;

/**
 * Created by zhengboyuan on 2016-12-29.
 */

public class PreferenceUtil {

    static public int getPrefInt(SharedPreferences sharedPrefs, String key, int defValue) {
        try {
            String text = sharedPrefs.getString(key, Integer.toString(defValue));
            return Integer.parseInt(text);
        } catch (Exception e) {
            return defValue;
        }
    }

    static public boolean getPrefBoolean(SharedPreferences sharedPrefs, String key, boolean defValue) {
        try {
            return sharedPrefs.getBoolean(key, defValue);
        } catch (Exception e) {
            //e.printStackTrace();
            return defValue;
        }
    }


}