package com.wits.util;

import android.net.Uri;

import java.util.List;

/**
 * Created by zhengboyuan on 2016-12-29.
 */

public interface ImageCache {

    public static interface OnImageReady {
        void handle(String uri, Uri localUri);
    }

    public boolean open(String cachePath);

    public void close();

    public void clear();

    public List<String> getCached();

    public void getImage(String uri, OnImageReady handler);

}
