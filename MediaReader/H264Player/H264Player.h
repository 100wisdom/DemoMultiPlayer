/*
 * H264Player.h
 *
 *  Created on: 2012-5-28
 *      Author: terry
 */

#ifndef H264PLAYER_H_
#define H264PLAYER_H_


#include <stddef.h>
#include <windef.h>
#include <time.h>
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#ifndef DLLEXPORT
#define DLLEXPORT __declspec(dllexport)
#endif //DLLEXPORT
#else
#define DLLEXPORT __attribute__ ((visibility ("default"))) 
#endif //WIN32
/////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

/////////////////////////////////////////////////////////////////////////////
enum StreamType
{
    kElementStream = 0,
    kProgramStream
};
/////////////////////////////////////////////////////////////////////////////

/**
 * 初始化解码库
 * @return TRUE 如果成功
 */
DLLEXPORT BOOL WINAPI H264_Init();


/**
 * 清理解码库
 */
DLLEXPORT void WINAPI H264_Uninit();


/**
 * 获取库的版本
 * @return 版本字符串
 */
DLLEXPORT LPCSTR WINAPI H264_GetVersion();


/**
 * 获取操作的错误码, 与windows错误码意义一致
 * @return 0 表示没有错误
 */
DLLEXPORT long WINAPI H264_GetLastError();


/**
 * 创建流式解码器
 * @param streamType @see StreamType
 * @return 解码器句柄， NULL表示失败， 需要调用H264_DestroyPlayer进行关闭
 */
DLLEXPORT HANDLE WINAPI H264_CreatePlayer(int streamType = kElementStream);

/**
 * 销毁解码器
 * @param handle 解码器句柄
 */
DLLEXPORT void WINAPI H264_DestroyPlayer(HANDLE handle);


/**
 * 设置显示窗口
 * @param handle 解码器句柄
 * @param hwnd  窗口句柄
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_SetVideoWnd(HANDLE handle, HWND hwnd);


/**
 * 输入视频数据
 * @param handle 解码器句柄
 * @param buffer 数据缓冲区
 * @param length 数据长度
 * @return TRUE 表示解码成功
 */
DLLEXPORT BOOL WINAPI H264_InputData(HANDLE handle, const BYTE* buffer, DWORD length);

/**
 * 获取视频分辨率，调用H264_InputData解码成功之后才会返回有效值
 * @param pWidth 宽
 * @param pHeight 高
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_GetPictureSize(HANDLE handle, int* pWidth, int* pHeight);

/**
 * 抓图，格式为BMP
 * @param handle 解码器句柄
 * @param filename 文件名
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_Snap(HANDLE handle, LPCSTR filename);

/**
 * 刷新窗口
 * @param handle 解码器句柄
 */
DLLEXPORT void WINAPI H264_Refresh(HANDLE handle);

/**
 * 播放
 * @param handle
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_Play(HANDLE handle);

/**
 * 暂停
 * @param handle
 * @param pauseIt TRUE 表示暂停，FALSE表示恢复
 * @param TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_Pause(HANDLE handle, BOOL pauseIt);

/**
 * 停止
 * @param handle 解码器句柄
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_Stop(HANDLE handle);


/**
 * 解码回调
 * @param handle 解码器句柄
 * @param pBuf 视频数据指针
 * @param size 视频数据长度
 * @param width 宽
 * @param height 高
 * @param fmt 视频帧格式
 * @param pUser 设置回调时传入的环境指针
 */
typedef void (* H264DecodeCallback)(HANDLE handle, unsigned char* pBuf, int size,
                                         int width, int height, int fmt,
                                         void* pUser);

/**
 * 显示回调
 * @param handle 解码器句柄
 * @param hdc 绘图设备环境句柄
 * @param pUser 设置回调时环境指针
 *
 */
typedef void (* H264DrawCallback)(HANDLE handle, HDC hdc, void* pUser);

/**
 * 设置解码回调
 * @param handle 解码句柄
 * @param cb 回调函数
 * @param pUser 环境指针
 *
 */
DLLEXPORT void WINAPI H264_SetDecodeCallback(HANDLE handle, H264DecodeCallback cb, void* pUser);

/**
 * 设置显示回调，可在回调中进行绘图操作
 * @param handle 解码器句柄
 * @param cb 回调函数
 * @param pUser 环境指针
 */
DLLEXPORT void WINAPI H264_SetDrawCallback(HANDLE handle, H264DrawCallback cb, void* pUser);





/**
 * 打开文件，支持.h264,.ts,.mp4,.avi格式
 * @param filename 文件名
 * @return 解码器句柄， 需要调用H264_CloseFile关闭
 */
DLLEXPORT HANDLE WINAPI H264_OpenFile(LPCSTR filename);


/**
 * 关闭文件解码器
 * @param handle 解码器句柄
 */
DLLEXPORT void WINAPI H264_CloseFile(HANDLE handle);

/**
 * 获取播放进度
 * @param handle
 * @return 播放进度，范围为[0, 1]
 */
DLLEXPORT double WINAPI H264_GetPos(HANDLE handle);

/**
 * 设置播放文件，即定位操作seek
 * @param handle 解码器句柄
 * @param pos 进度值，范围为[0, 1]
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_SetPos(HANDLE handle, double pos);

/**
 * 获取文件的总时长,
 * @param handle 解码器句柄
 * @param pSeconds 总时长，单位为秒，有些文件无法获取时长，比如.h264格式的文件
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_GetDuration(HANDLE handle, double* pSeconds);

/**
 * 设置播放速度，该操作对实时解码无效
 * @param handle 解码器句柄
 * @param scale 播放速度，范围为[1/8, 8], 1表示正常速度，8 表示快八倍速，1/8表示慢八倍速
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_SetScale(HANDLE handle, double scale);


/**
 * 设置文件播放结束通知消息，消息的wparam表示句柄，lparam保留
 * @param handle 解码器句柄
 * @param hwnd 消息窗口句柄
 * @param msg 消息类型
 * @return TRUE 表示成功
 */
DLLEXPORT BOOL WINAPI H264_SetFileEndMsg(HANDLE handle, HWND hwnd, UINT msg);




/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif


#endif /* H264PLAYER_H_ */
