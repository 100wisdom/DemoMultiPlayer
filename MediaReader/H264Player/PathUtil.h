/*
 * PathUtil.h
 *
 *  Created: 2011-10-21
 *   Author: terry
 */


#if !defined PATHUTIL_TERRY_
#define PATHUTIL_TERRY_


#include <vector>
#include <string>
using std::string;


////////////////////////////////////////////////////////////////////////////
class PathUtil
{
public:
    enum
    {
        PATH_SPERATOR = '\\',
        FORWARD_SLASH = '/',
        BACKWARD_SLASH = '\\'

    };

public:
    static bool exists(const string& path);
    static bool createDirectory(const string& path);
    static bool createDirectories(const string& path);
    static bool isFile(const string& path);
    static bool isDir(const string& path);
    static bool isLink(const string& path);
    static bool remove(const string& path);

    static time_t getatime(const string& path);
    static time_t getmtime(const string& path);
    static time_t getctime(const string& path);

    static __int64 getSize(const string& path);


    static bool isValidFileName(const string& filename);
    static string abspath(const string& path);
    static bool isabs(const string& path);

    static string basename(const string& path);
    static string dirname(const string& path);


    static string join(const string& dir, const string& filename);

    static string getFileName(const string& path);

    /// get file extension, such as ".txt"
    static string getFileExt(const string& filename);

    static string getFileTitle(const string& filename);




    /**
     * Normalize a pathname.
     * A//B, A/./B and A/foo/../B all become A/B.
     */
    static string normpath(const string& path);

    /**
     * Split the pathname path into a pair, (head, tail)
     */
    static std::pair< string, string > split(const string& path);

    /**
     * Split the pathname path into a pair (root, ext)
     *  such that root + ext == path
     */
    static std::pair< string, string > splitext(const string& path);


    static bool listDir(const string& dir, std::vector< string >& array);
    static bool listFile(const string& dir, std::vector< string >& array);


    static bool addSperator(string& path);
    static void removeSperator(string& path);
    static bool endWithSperator(const string& path);

    static void convertSlash(string& path);
    static void toWindowsSlash(string& path);
	static void toUnixSlash(string& path);


    static string getCurDir();
    static string getWorkDir();

    static string getWindowDir();
    static string getWindowDisk();
    static string getSystemDir();
    static string getSystem32Dir();

    static string getHomeDir();

};

////////////////////////////////////////////////////////////////////////////
#endif //PATHUTIL_TERRY_
