
#ifdef MONITOR_EXPORTS
#define MONITOR_API __declspec(dllexport)
#else
#define MONITOR_API __declspec(dllimport)
#endif

/**
 *	解码库，打开流接口。
 *
 *	@param nPort 输出参数，工作端口号用来标识视频流
 *
 *	@return TRUE表示成功，FALSE表示失败
 *
 *	@note 解码库，打开流接口。
 */	
MONITOR_API bool XX_DEC_OpenStream(long *nPort);

/**
 *  解码库，视频显示。需要在打开流接口之后才能输入数据。
 *
 *  @param nPort 工作端口号，视频流标识。
 *  @param hPlayWnd 视频显示窗口。
 *
 *  @return TRUE表示成功，FASLE表示失败。
 */
MONITOR_API bool XX_DEC_DisplayStream(long nPort, HWND hPlayWnd);

/**
 *	解码库，输入流数据。需要在打开流接口之后才能输入数据。
 *
 *	@param nPort 工作端口号，视频流标识。
 *	@param pBuf 流数据缓冲区地址
 *	@param nSize 流数据缓冲区大小
 *
 *	@return TRUE表示成功。FALSE表示失败，没有数据输入
 *
 *	@note 解码库，输入流数据。需要在开启流之后才能输入数据
 */
MONITOR_API bool XX_DEC_InputData(
					long nPort,
					char *pBuf,
					long nSize);

/**
 *	视频回调函数
 *
 *	@param nPort 工作端口号
 *	@param pYUVBuf 解码后的视频数据
 *	@param nSize 解码后的视频数据pYUVBuf的长度
 *	@param nWidth 画面宽
 *	@param nHeight 画面高
 *	@param nStamp 时间戳信息
 *	@param nUser 用户自定义数据
 *
 *	@return 无
 */
typedef void (*VideoCallBack)(
				long nPort,
				char *pYUVBuf,
				long nSize,
				long nWidth,
				long nHeight,
				long nStamp,
				long nUser);

/**
 *	解码库，设置视频回调函数，给客户端提供YUV数据。
 *
 *	@param nPort 工作端口号
 *	@param fVideoCallBack 回调函数
 *	@param nUser 用户自定义数据
 *
 *	@return TRUE表示成功，FALSE表示失败
 *
 *	@note 设置回调函数，用户自己处理客户端收到的视频数据。
 */
MONITOR_API bool XX_DEC_SetVideoCallBack(
					long nPort,
					VideoCallBack fVideoCallBack,
					long nUser);

/**
 *	音频回调函数
 *
 *	@param nPort 工作端口号
 *	@param pAudioBuf 解码后的音频数据
 *	@param nSize 解码后的音频数据pAudioBuf的长度
 *	@param nStamp 时间戳信息
 *	@param nUser 用户自定义数据
 *
 *	@return 无
 */
typedef void (*AudioCallBack)(
				long nPort,
				char *pAudioBuf,
				long nSize,
				long nStamp,
				long nUser);

/**
 *	解码库，设置音频回调函数，给客户端提供音频数据。
 *
 *	@param nPort 工作端口号
 *	@param fAudioCallBack 回调函数
 *	@param nUser 用户自定义数据
 *
 *	@return TRUE表示成功，FALSE表示失败
 *
 *	@note 设置回调函数，用户自己处理客户端收到的视频数据。
 */
MONITOR_API bool XX_DEC_SetAudioCallBack(
					long nPort,
					AudioCallBack fAudioCallBack,
					long nUser);

/**
 *	解码库，关闭流接口。
 *
 *	@param nPort 工作端口号
 *
 *	@return TRUE表示成功，FALSE表示失败
 *
 *	@note 解码库，关闭流接口
 */
MONITOR_API bool XX_DEC_CloseStream(long nPort);