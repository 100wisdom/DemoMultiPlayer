/*
 * H264FilePlayer.h
 *
 *  Created: 2012-05-29
 *   Author: terry
 */


#if !defined H264FILEPLAYER_TERRY_
#define H264FILEPLAYER_TERRY_

#include "BaseH264Player.h"
#include <string>
#include "H264FileReader.h"
#include "TThread.h"
#include "TEvent.h"
#include "TCriticalSection.h"
////////////////////////////////////////////////////////////////////////////
class H264FilePlayer : public BaseH264Player, public comn::Thread
{
public:
    H264FilePlayer(const char* filename);
    ~H264FilePlayer();

    bool setFileName(const char* filename);

    virtual bool open();
    virtual void close();

    virtual bool play();
    virtual bool pause(bool toPause);
    virtual void stopStream();

    virtual double getPos();
    virtual bool setPos(double pos);
    virtual double getDuration();

    virtual bool setScale(double scale);

protected:
    virtual int run();
    virtual bool startup();
    virtual void cleanup();
    virtual void doStop();

    bool dealFrame();

private:
    std::string m_filename;

    H264FileReader  m_reader;

    bool    m_paused;
    double  m_scale;

    DWORD   m_frameCount;

    comn::Event m_event;
    comn::CriticalSection   m_cs;

};

////////////////////////////////////////////////////////////////////////////
#endif //H264FILEPLAYER_TERRY_
