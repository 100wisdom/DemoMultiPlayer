/*
 * H264MediaPlayer.h
 *
 *  Created: 2012-05-29
 *   Author: terry
 */


#if !defined H264MEDIAPLAYER_TERRY_
#define H264MEDIAPLAYER_TERRY_

#include "H264Player.h"
////////////////////////////////////////////////////////////////////////////
class H264MediaPlayer
{
public:
    virtual ~H264MediaPlayer() {}
    
    virtual bool open() =0;
    virtual void close() =0;

    virtual bool setVideoWnd(HWND hwnd) =0;

    virtual bool setPropSet(const unsigned char* buffer, size_t length) =0;
    virtual bool inputData(const unsigned char* buffer, size_t length) =0;

    virtual bool play() =0;
    virtual bool pause(bool toPause) =0;
    virtual void stopStream() =0;

    virtual double getPos() =0;
    virtual bool setPos(double pos) =0;
    virtual double getDuration() =0;

    virtual bool setScale(double scale) =0;

    virtual bool snap(const char* filename) =0;

    virtual void refresh() =0;

    virtual void setDecodeCallback(HANDLE handle, H264DecodeCallback cb, void* pUser) =0;
    virtual void setDrawCallback(HANDLE handle, H264DrawCallback cb, void* pUser) =0;

    virtual bool getPictureSize(int& width, int& height) =0;

    virtual void resetBuffer() =0;

    virtual bool setFileEndMsg(HWND hwnd, UINT msg) =0;


};

////////////////////////////////////////////////////////////////////////////
#endif //H264MEDIAPLAYER_TERRY_
