/*
 * HandleMap.h
 *
 *  Created: 2012-05-29
 *   Author: terry
 */


#if !defined HANDLEMAP_TERRY_
#define HANDLEMAP_TERRY_

#include "H264MediaPlayer.h"
#include <map>
#include "TCriticalSection.h"
////////////////////////////////////////////////////////////////////////////

typedef std::map< HANDLE, H264MediaPlayer* >       BaseHandleMap;

class HandleMap
{
public:
    HandleMap();
    ~HandleMap();

    bool exists(HANDLE handle);

    H264MediaPlayer* find(HANDLE handle);

    bool add(HANDLE handle, H264MediaPlayer* pPlayer);
    
    bool remove(HANDLE handle);

    void deleteAll();

private:
    BaseHandleMap   m_cont;
    comn::CriticalSection   m_cs;

};

////////////////////////////////////////////////////////////////////////////
#endif //HANDLEMAP_TERRY_
