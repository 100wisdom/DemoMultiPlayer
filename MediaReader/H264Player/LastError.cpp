#include "StdAfx.h"
#include "LastError.h"


int  LastError::s_code = 0;

LastError::LastError(void)
{
}

LastError::~LastError(void)
{
}


int LastError::get()
{
    return s_code;
}

bool LastError::set(int code)
{
    s_code = code;
    return (s_code == 0);
}

bool LastError::setSuccess()
{
    s_code = 0;
    return TRUE;
}

