/*
 * WinTrace.cpp
 *
 *  Created: 2011-12-12
 *   Author: terry
 */

#include "stdafx.h"
#include "WinTrace.h"
#include <stdio.h>


void WinTrace(LPCTSTR fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    
    TCHAR buffer[1024];
    int len = vsprintf_s(buffer, 1024, fmt, ap);
    if (len >= 0)
    {
        buffer[len] = '\0';

        OutputDebugString(buffer);
    }

    va_end(ap);
}
