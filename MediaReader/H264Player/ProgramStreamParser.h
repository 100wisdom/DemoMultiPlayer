/*
 * ProgramStreamParser.h
 *
 *  Created: 2013-07-07
 *   Author: terry
 */


#if !defined PROGRAMSTREAMPARSER_TERRY_
#define PROGRAMSTREAMPARSER_TERRY_

#include "TIOBuffer.h"
#include "NaluAnalyzer.h"
////////////////////////////////////////////////////////////////////////////

class ProgramStreamParser
{
public:
    typedef unsigned char   uint8_t;
    
    struct StreamPacket
    {
        uint8_t*    data;
        int length;
        uint8_t type;
    };

public:
    ProgramStreamParser();
    ~ProgramStreamParser();

    bool inputData(const uint8_t* buffer, size_t length);

    void setSink(NaluAnalyzerSink* pSink);

    void clear();

protected:
    void onNewPacket(StreamPacket& packet);
    void onPESPacket(StreamPacket& packet);
    void writePacket(NaluPacket& packet);
    void flushCompoundBuffer();

    bool findPacket(uint8_t* buffer, size_t length, size_t start, NaluPacket& packet);

    bool findPacket(uint8_t* buffer, size_t length, size_t start, StreamPacket& packet);

    bool isStreamID(uint8_t ch);

    bool searchPrefix(uint8_t* buffer, size_t length, int& prefix);

private:
    NaluAnalyzerSink*   m_pSink;
    comn::IOBuffer      m_buffer;

    uint8_t m_lastType;
    comn::IOBuffer    m_compoundBuffer;
};

////////////////////////////////////////////////////////////////////////////
#endif //PROGRAMSTREAMPARSER_TERRY_
