/*
 * ffmpeg.h
 *
 *  Created on: 2011-4-29
 *      Author: terry
 */

#ifndef FFMPEG_H_
#define FFMPEG_H_

#ifdef _MSC_VER
    #pragma warning(disable: 4244)
#endif //WIN32


#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif


extern "C"
{
    #include "libavformat/avformat.h"
    #include "libavutil/avutil.h"
    #include "libavutil/base64.h"
    #include "libavutil/rational.h"
    #include "libswscale/swscale.h"
}



#endif /* FFMPEG_H_ */
