/*
 * H264ProgramStreamPlayer.h
 *
 *  Created: 2013-07-07
 *   Author: terry
 */


#if !defined H264PROGRAMSTREAMPLAYER_TERRY_
#define H264PROGRAMSTREAMPLAYER_TERRY_

#include "BaseH264Player.h"
#include "TIOBuffer.h"
#include "ProgramStreamParser.h"
////////////////////////////////////////////////////////////////////////////
class H264ProgramStreamPlayer : public BaseH264Player, public NaluAnalyzerSink
{
public:
    H264ProgramStreamPlayer();
    ~H264ProgramStreamPlayer();

    virtual bool inputData(const unsigned char* buffer, size_t length);

    virtual void resetBuffer();

protected:
    virtual void writePacket(NaluPacket& packet);

protected:
    ProgramStreamParser    m_analyzer;
    comn::IOBuffer  m_buffer;

};

////////////////////////////////////////////////////////////////////////////
#endif //H264PROGRAMSTREAMPLAYER_TERRY_
