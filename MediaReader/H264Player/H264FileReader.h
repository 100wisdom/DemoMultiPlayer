/*
 * H264FileReader.h
 *
 *  Created: 2012-05-10
 *   Author: terry
 */


#if !defined H264FILEREADER_TERRY_
#define H264FILEREADER_TERRY_

#include <string>
////////////////////////////////////////////////////////////////////////////


class H264FileReader
{
public:
    typedef unsigned char   uint8_t;

    class H264NaluPacket
    {
    public:
        uint8_t type;
        uint8_t*    data;
        int length;
        int ts;

        H264NaluPacket():
        type(),
            data(),
            length(),
            ts()
        {
        }

    };

public:
    H264FileReader();
    ~H264FileReader();

    bool open(const char* filename);

    void close();

    bool isOpen();

    bool getPropSet(std::string& sps, std::string& pps);

    bool read(H264NaluPacket& nalu);

    
    long getTotal();

    long getPos();

    bool setPos(long pos);

    
private:

    struct Pimpl;

    Pimpl*  m_pPimpl;

    long  m_duration;
    long  m_pos;

};

////////////////////////////////////////////////////////////////////////////
#endif //H264FILEREADER_TERRY_
