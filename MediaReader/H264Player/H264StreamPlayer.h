/*
 * H264StreamPlayer.h
 *
 *  Created: 2012-05-29
 *   Author: terry
 */


#if !defined H264STREAMPLAYER_TERRY_
#define H264STREAMPLAYER_TERRY_

#include "BaseH264Player.h"
#include "NaluAnalyzer.h"
////////////////////////////////////////////////////////////////////////////
class H264StreamPlayer : public BaseH264Player, public NaluAnalyzerSink
{
public:
    H264StreamPlayer();
    ~H264StreamPlayer();

    virtual bool inputData(const unsigned char* buffer, size_t length);

    virtual void resetBuffer();

protected:
    virtual void writePacket(NaluPacket& packet);

	void flushBuffer();

protected:
    NaluAnalyzer    m_analyzer;
    comn::IOBuffer  m_buffer;

};

////////////////////////////////////////////////////////////////////////////
#endif //H264STREAMPLAYER_TERRY_
