// H264Player.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include "H264Player.h"
#include "H264Decode.h"

#include "LastError.h"
#include "HandleMap.h"

#include "H264FilePlayer.h"
#include "H264StreamPlayer.h"
#include "H264ProgramStreamPlayer.h"


HandleMap   g_handleMap;


#define CheckPointer(p, result)     { if (!p)  {LastError::set(ERROR_INVALID_HANDLE); return result;} }

#define CheckPointerReturnVoid(p)   { if (!p)  {LastError::set(ERROR_INVALID_HANDLE); return ;} }


bool CheckResult(bool result)
{
    int code = result ? 0 : ERROR_GEN_FAILURE;
    LastError::set(code);
    return result;
};




/**
 * 初始化解码库
 * @return TRUE 如果成功
 */
BOOL WINAPI H264_Init()
{
    H264Decode::startup();

    return TRUE;
}


/**
 * 清理解码库
 */
void WINAPI H264_Uninit()
{
    g_handleMap.deleteAll();
}


/**
 * 获取库的版本
 * @return 版本字符串
 */
LPCSTR WINAPI H264_GetVersion()
{
    return "H264Player 1.0";
}


/**
 * 获取操作的错误码, 与windows错误码意义一致
 * @return 0 表示没有错误
 */
long WINAPI H264_GetLastError()
{
    return LastError::get();
}


/**
 * 创建流式解码器
 * @param streamType @see StreamType
 * @return 解码器句柄， NULL表示失败， 需要调用H264_DestroyPlayer进行关闭
 */
HANDLE WINAPI H264_CreatePlayer(int streamType)
{
    BaseH264Player* pPlayer = NULL;
    if (streamType == kProgramStream)
    {
        pPlayer = new H264ProgramStreamPlayer();
    }
    else
    {
        pPlayer = new H264StreamPlayer();
    }
    
    if (!pPlayer->open())
    {
        delete pPlayer;
        pPlayer = NULL;
        return NULL;
    }

    g_handleMap.add(pPlayer, pPlayer);
    return pPlayer;
}

/**
 * 销毁解码器
 * @param handle 解码器句柄
 */
void WINAPI H264_DestroyPlayer(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointerReturnVoid(pPlayer);

    g_handleMap.remove(handle);

    pPlayer->close();
    delete pPlayer;
}


/**
 * 设置显示窗口
 * @param handle 解码器句柄
 * @param hwnd  窗口句柄
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_SetVideoWnd(HANDLE handle, HWND hwnd)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->setVideoWnd(hwnd);
    return CheckResult(done);
}


/**
 * 输入视频数据
 * @param handle 解码器句柄
 * @param buffer 数据缓冲区
 * @param length 数据长度
 * @return TRUE 表示解码成功
 */
BOOL WINAPI H264_InputData(HANDLE handle, const BYTE* buffer, DWORD length)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->inputData(buffer, length);
    return CheckResult(done);
}

/**
 * 获取视频分辨率，调用H264_InputData解码成功之后才会返回有效值
 * @param pWidth 宽
 * @param pHeight 高
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_GetPictureSize(HANDLE handle, int* pWidth, int* pHeight)
{
    CheckPointer(pWidth, FALSE);
    CheckPointer(pHeight, FALSE);

    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->getPictureSize(*pWidth, *pHeight);
    return CheckResult(done);
}

/**
 * 抓图，格式为BMP
 * @param filename 文件名
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_Snap(HANDLE handle, LPCSTR filename)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->snap(filename);
    return CheckResult(done);
}

/**
 * 刷新窗口
 */
void WINAPI H264_Refresh(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointerReturnVoid(pPlayer);

    pPlayer->refresh();
}

/**
 * 播放
 * @param handle
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_Play(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->play();
    return CheckResult(done);
}

/**
 * 暂停
 * @param handle
 * @param pauseIt TRUE 表示暂停，FALSE表示恢复
 * @param TRUE 表示成功
 */
BOOL WINAPI H264_Pause(HANDLE handle, BOOL pauseIt)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->pause(pauseIt == TRUE);
    return CheckResult(done);
}

/**
 * 停止
 * @param handle 解码器句柄
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_Stop(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    pPlayer->stopStream();
    return TRUE;
}



/**
 * 设置解码回调
 * @param handle 解码句柄
 * @param cb 回调函数
 * @param pUser 环境指针
 *
 */
void WINAPI H264_SetDecodeCallback(HANDLE handle, H264DecodeCallback cb, void* pUser)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointerReturnVoid(pPlayer);

    pPlayer->setDecodeCallback(handle, cb, pUser);
}

/**
 * 设置显示回调，可在回调中进行绘图操作
 * @param handle 解码器句柄
 * @param cb 回调函数
 * @param pUser 环境指针
 */
void WINAPI H264_SetDrawCallback(HANDLE handle, H264DrawCallback cb, void* pUser)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointerReturnVoid(pPlayer);

    pPlayer->setDrawCallback(handle, cb, pUser);
}





/**
 * 打开文件，支持.h264,.ts,.mp4,.avi格式
 * @param filename 文件名
 * @return 解码器句柄， 需要调用H264_CloseFile关闭
 */
HANDLE WINAPI H264_OpenFile(LPCSTR filename)
{
    H264FilePlayer* pPlayer = new H264FilePlayer(filename);
    if (!pPlayer->open())
    {
        delete pPlayer;
        pPlayer = NULL;
    }

    g_handleMap.add(pPlayer, pPlayer);

    return pPlayer;
}


/**
 * 关闭文件解码器
 * @param handle 解码器句柄
 */
void WINAPI H264_CloseFile(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointerReturnVoid(pPlayer);

    pPlayer->stopStream();

    g_handleMap.remove(handle);

    pPlayer->close();
    delete pPlayer;
}

/**
 * 获取播放进度
 * @param handle
 * @return 播放进度，范围为[0, 1]
 */
double WINAPI H264_GetPos(HANDLE handle)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, 0);

    return pPlayer->getPos();
}

/**
 * 设置播放文件，即定位操作seek
 * @param handle 解码器句柄
 * @param pos 进度值，范围为[0, 1]
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_SetPos(HANDLE handle, double pos)
{
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->setPos(pos);
    return CheckResult(done);
}

/**
 * 获取文件的总时长,
 * @param handle 解码器句柄
 * @param pSeconds 总时长，单位为秒，有些文件无法获取时长，比如.h264格式的文件
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_GetDuration(HANDLE handle, double* pSeconds)
{
    CheckPointer(pSeconds, FALSE);
    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    *pSeconds = pPlayer->getDuration();

    return TRUE;
}

/**
 * 设置播放速度，该操作对实时解码无效
 * @param handle 解码器句柄
 * @param scale 播放速度，范围为[1/8, 8], 1表示正常速度，8 表示快八倍速，1/8表示慢八倍速
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_SetScale(HANDLE handle, double scale)
{
    if (scale < 1.0 / 8)
    {
        return LastError::set(ERROR_INVALID_PARAMETER);
    }
    if (scale > 8)
    {
        return LastError::set(ERROR_INVALID_PARAMETER);
    }

    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->setScale(scale);
    return CheckResult(done);
}


/**
 * 设置文件播放结束通知消息，消息的wparam表示句柄，lparam保留
 * @param handle 解码器句柄
 * @param hwnd 消息窗口句柄
 * @param msg 消息类型
 * @return TRUE 表示成功
 */
BOOL WINAPI H264_SetFileEndMsg(HANDLE handle, HWND hwnd, UINT msg)
{
    if (!::IsWindow(hwnd))
    {
        return LastError::set(ERROR_INVALID_PARAMETER);
    }

    if (msg == 0)
    {
        return LastError::set(ERROR_INVALID_PARAMETER);
    }

    H264MediaPlayer* pPlayer = g_handleMap.find(handle);
    CheckPointer(pPlayer, FALSE);

    bool done = pPlayer->setFileEndMsg(hwnd, msg);
    return CheckResult(done);
}


