CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

include_directories(${PROJECT_ROOT})
include_directories(${PROJECT_ROOT}/comn/include)
include_directories(.)

include_directories(${FFMPEG_DIR}/include)

if (MSVC)
	include_directories(${THIRD_PARTY}/stdint)
endif()

link_directories(${PROJECT_ROOT}/comn/lib)
link_directories(${PROJECT_ROOT}/lib)
link_directories(${FFMPEG_LIB_DIR})


FILE(GLOB HEADER "*.h")
aux_source_directory(. SOURCE)

if (MSVC)
    
else()
    list(REMOVE_ITEM SOURCE ./dllmain.cpp)
endif()


add_library(H264Player SHARED ${SOURCE} ${HEADER})

target_link_libraries(H264Player
    debug comnd optimized comn
    avcodec avformat swscale avutil avfilter swresample
	${LIB_PLATFORM}
	)

set_target_properties(H264Player PROPERTIES LINK_INTERFACE_LIBRARIES "")
set_target_properties(H264Player PROPERTIES INTERFACE_LINK_LIBRARIES "")
set_target_properties(H264Player PROPERTIES LINK_FLAGS_RELEASE "/OPT:NOREF")

