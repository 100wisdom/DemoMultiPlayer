/*
 * BaseH264Player.h
 *
 *  Created: 2012-05-29
 *   Author: terry
 */


#if !defined BASEH264PLAYER_TERRY_
#define BASEH264PLAYER_TERRY_

#include "H264MediaPlayer.h"
#include "H264Decode.h"
////////////////////////////////////////////////////////////////////////////
class BaseH264Player : public H264MediaPlayer
{
public:
    BaseH264Player();
    virtual ~BaseH264Player();

    virtual bool open();
    virtual void close();

    virtual bool setVideoWnd(HWND hwnd);

    virtual bool setPropSet(const unsigned char* buffer, size_t length);
    virtual bool inputData(const unsigned char* buffer, size_t length);

    virtual bool play();
    virtual bool pause(bool toPause);
    virtual void stopStream();

    virtual double getPos();
    virtual bool setPos(double pos);
    virtual double getDuration();

    virtual bool setScale(double scale);

    virtual bool snap(const char* filename);

    virtual void refresh();

    virtual void setDecodeCallback(HANDLE handle, H264DecodeCallback cb, void* pUser);
    virtual void setDrawCallback(HANDLE handle, H264DrawCallback cb, void* pUser);

    virtual bool getPictureSize(int& width, int& height);

    virtual void resetBuffer();

    virtual bool setFileEndMsg(HWND hwnd, UINT msg);

public:
    void videoFrameCallback(unsigned char* pBuf, int size,
        int width, int height, int fmt);

    void videoDrawCallback(HDC hdc);


protected:
    void postFileEndMsg();

protected:
    H264Decode  m_decode;

    HANDLE  m_handleCB;
    H264DecodeCallback  m_decodeCB;
    void*   m_pDecodeUser;
    H264DrawCallback    m_drawCB;
    void*   m_pDrawUser;

    HWND    m_msgWnd;
    UINT    m_endMsg;

};

////////////////////////////////////////////////////////////////////////////
#endif //BASEH264PLAYER_TERRY_
