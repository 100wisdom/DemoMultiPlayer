
#ifndef TDUMPFILE_H_
#define TDUMPFILE_H_

#include <string>

class DumpFile
{
public:
    static bool write(const char* data, size_t length, const char* filepath, bool append);

    static bool dump(const std::string& data, const char* filepath);

    static bool append(const std::string& data, const char* filepath);

};



bool DumpFile::write(const char* data, size_t length, const char* filepath, bool append)
{
    FILE* pFile = NULL;
    if (append)
    {
        pFile = fopen(filepath, "ab");
    }
    else
    {
        pFile = fopen(filepath, "wb");
    }

    if (!pFile)
    {
        return false;
    }

    fwrite(data, 1, length, pFile);


    fclose(pFile);
    return true;
}

bool DumpFile::dump(const std::string& data, const char* filepath)
{
    return write(data.c_str(), data.length(), filepath, true);
}


bool DumpFile::append(const std::string& data, const char* filepath)
{
    return write(data.c_str(), data.length(), filepath, true);
}



#endif //TDUMPFILE_H_