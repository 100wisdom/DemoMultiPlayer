/*
 * NaluAnalyzer.h
 *
 *  Created: 2012-05-10
 *   Author: terry
 */


#if !defined NALUANALYZER_TERRY_
#define NALUANALYZER_TERRY_

#include "TIOBuffer.h"
////////////////////////////////////////////////////////////////////////////

typedef unsigned char uint8_t;



struct NaluPacket
{
    enum NaluType
    {
        NALU_NULL = 0,
        NALU_SPS = 7,
        NALU_PPS = 8,
        NALU_SEI = 6,
        NALU_IFRAME = 5
    };

    uint8_t*    data;
    int length;
    uint8_t type;
    int prefix;
};


class NaluAnalyzerSink
{
public:
    virtual ~NaluAnalyzerSink() {}

    virtual void writePacket(NaluPacket& packet) =0;

};



class NaluAnalyzer
{
public:
    NaluAnalyzer();
    ~NaluAnalyzer();

    bool inputData(const uint8_t* buffer, size_t length);

    void setSink(NaluAnalyzerSink* pSink);

    void clear();

protected:
    void onNewPacket(NaluPacket& packet);
    void writePacket(NaluPacket& packet);
    void flushCompoundBuffer();

    bool findNalu(uint8_t* buffer, size_t length, size_t start, NaluPacket& packet);

private:
    NaluAnalyzerSink*   m_pSink;
    comn::IOBuffer    m_buffer;

    uint8_t m_lastType;
    comn::IOBuffer    m_compoundBuffer;

};

////////////////////////////////////////////////////////////////////////////
#endif //NALUANALYZER_TERRY_
