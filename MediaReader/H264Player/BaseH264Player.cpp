/*
 * BaseH264Player.cpp
 *
 *  Created: 2012-05-29
 *   Author: terry
 */

#include "stdafx.h"
#include "BaseH264Player.h"


void Player_H264VideoFrameCallback(unsigned char* pBuf, int size,
                                   int width, int height, int fmt,
                                   void* pUser)
{
    BaseH264Player* pPlayer = (BaseH264Player*)pUser;
    pPlayer->videoFrameCallback(pBuf, size, width, height, fmt);
}

void Player_H264VideoDrawCallback(HDC hdc, void* pUser)
{
    BaseH264Player* pPlayer = (BaseH264Player*)pUser;
    pPlayer->videoDrawCallback(hdc);
}


BaseH264Player::BaseH264Player():
    m_decode()
{
    m_handleCB = NULL;
    m_decodeCB = NULL;
    m_pDecodeUser = NULL;
    m_drawCB = NULL;
    m_pDrawUser = NULL;

    m_msgWnd = 0;
    m_endMsg = 0;
}

BaseH264Player::~BaseH264Player()
{

}

bool BaseH264Player::open()
{
    return m_decode.open();
}

void BaseH264Player::close()
{
    m_decode.close();
}

bool BaseH264Player::setVideoWnd(HWND hwnd)
{
    return m_decode.setVideoWnd(hwnd);
}

bool BaseH264Player::setPropSet(const unsigned char* buffer, size_t length)
{
    return m_decode.inputData(buffer, length);
}

bool BaseH264Player::inputData(const unsigned char* buffer, size_t length)
{
    return m_decode.inputData(buffer, length);
}

bool BaseH264Player::play()
{
    return true;
}

bool BaseH264Player::pause(bool toPause)
{
    return true;
}

void BaseH264Player::stopStream()
{
    // pass
}

double BaseH264Player::getPos()
{
    return 0;
}

bool BaseH264Player::setPos(double pos)
{
    return false;
}

double BaseH264Player::getDuration()
{
    return 0;
}

bool BaseH264Player::setScale(double scale)
{
    return false;
}

bool BaseH264Player::snap(const char* filename)
{
    return m_decode.snap(filename);
}

void BaseH264Player::refresh()
{
    return m_decode.refresh();
}

void BaseH264Player::setDecodeCallback(HANDLE handle, H264DecodeCallback cb, void* pUser)
{
    m_handleCB = handle;
    m_decodeCB = cb;
    m_pDecodeUser = pUser;
    m_decode.setDecodeCallback(Player_H264VideoFrameCallback, this);
}

void BaseH264Player::setDrawCallback(HANDLE handle, H264DrawCallback cb, void* pUser)
{
    m_handleCB = handle;
    m_drawCB = cb;
    m_pDrawUser = pUser;
    m_decode.setDrawCallback(Player_H264VideoDrawCallback, this);
}

bool BaseH264Player::getPictureSize(int& width, int& height)
{
    return m_decode.getPictureSize(width, height);
}

void BaseH264Player::resetBuffer()
{
    // pass
}

bool BaseH264Player::setFileEndMsg(HWND hwnd, UINT msg)
{
    if (!::IsWindow(hwnd))
    {
        return false;
    }

    if (msg == 0)
    {
        return false;
    }

    m_msgWnd = hwnd;
    m_endMsg = msg;
    return true;
}

void BaseH264Player::videoFrameCallback(unsigned char* pBuf, int size,
                        int width, int height, int fmt)
{
    (*m_decodeCB)(m_handleCB, pBuf, size, width, height, fmt, m_pDecodeUser);
}

void BaseH264Player::videoDrawCallback(HDC hdc)
{
    (*m_drawCB)(m_handleCB, hdc, m_pDrawUser);
}

void BaseH264Player::postFileEndMsg()
{
    if (::IsWindow(m_msgWnd))
    {
        ::PostMessage(m_msgWnd, m_endMsg, (WPARAM)(HANDLE)this, 0);
    }
}