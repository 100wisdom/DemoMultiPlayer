#pragma once

class LastError
{
public:
    LastError(void);
    ~LastError(void);

    static int get();
    
    static bool set(int code);

    static bool setSuccess();

private:
    static int  s_code;

};
