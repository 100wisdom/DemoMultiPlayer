
// DemoH264PlayerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DemoH264Player.h"
#include "DemoH264PlayerDlg.h"

#pragma warning(disable: 4996)


#define     WM_FILE_END     (WM_USER + 101)


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDemoH264PlayerDlg 对话框




CDemoH264PlayerDlg::CDemoH264PlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDemoH264PlayerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

    m_handle = NULL;
    m_isFilePlayer = false;
    m_pFile = NULL;
}

void CDemoH264PlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDemoH264PlayerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_WM_SIZE()
    ON_BN_CLICKED(IDC_BTN_FILE, &CDemoH264PlayerDlg::OnBnClickedBtnFile)
    ON_BN_CLICKED(IDC_BTN_PLAY, &CDemoH264PlayerDlg::OnBnClickedBtnPlay)
    ON_BN_CLICKED(IDC_BTN_PAUSE, &CDemoH264PlayerDlg::OnBnClickedBtnPause)
    ON_BN_CLICKED(IDC_BTN_STOP, &CDemoH264PlayerDlg::OnBnClickedBtnStop)
    ON_BN_CLICKED(IDC_BTN_SNAP, &CDemoH264PlayerDlg::OnBnClickedBtnSnap)
    ON_BN_CLICKED(IDC_BTN_LIVE, &CDemoH264PlayerDlg::OnBnClickedBtnLive)
    ON_MESSAGE(WM_FILE_END, OnFileEndMsg)
    ON_WM_DESTROY()
    ON_WM_TIMER()
END_MESSAGE_MAP()


// CDemoH264PlayerDlg 消息处理程序

BOOL CDemoH264PlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	layout();

    init();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CDemoH264PlayerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CDemoH264PlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CDemoH264PlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDemoH264PlayerDlg::layout()
{
    CRect rc;
    GetClientRect(&rc);
    rc.DeflateRect(2, 2);

    CRect rcVideo(rc);
    rcVideo.bottom -= 34;

    CWnd* pVideoWnd = GetDlgItem(IDC_VIDEO_FRAME);
    if (pVideoWnd)
    {
        pVideoWnd->MoveWindow(rcVideo);
    }

    CRect rcBar(rc);
    rcBar.top = rcVideo.bottom + 2;

    layoutBtn(rcBar);
}

void CDemoH264PlayerDlg::layoutBtn(const CRect& rcBar)
{
    int space = 4;
    UINT ids[] = {IDC_BTN_FILE, IDC_BTN_PLAY, IDC_BTN_PAUSE,
                    IDC_BTN_STOP, IDC_BTN_SNAP, IDC_BTN_LIVE, 0};
    size_t count = sizeof(ids)/sizeof(ids[0]);
    int btnWidth = (rcBar.Width() - (count - 1) * space) / count;
    int btnHeight = rcBar.Height() - space;

    CRect rcBtn;
    rcBtn.left = rcBar.left;
    rcBtn.right = rcBtn.left + btnWidth;
    rcBtn.top = rcBar.top + (rcBar.Height() - btnHeight)/2;
    rcBtn.bottom = rcBtn.top + btnHeight;

    for (size_t i = 0; i < count; ++ i)
    {
        CWnd* pWnd = GetDlgItem(ids[i]);
        if (pWnd)
        {
            pWnd->MoveWindow(rcBtn);
        }

        rcBtn.OffsetRect(btnWidth + space, 0);
    }
}

void CDemoH264PlayerDlg::init()
{
    layout();

    H264_Init();
}

void CDemoH264PlayerDlg::uninit()
{
    KillTimer(100);

    closePlayer();

    H264_Uninit();
}

void CDemoH264PlayerDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);

    layout();

}

void CDemoH264PlayerDlg::OnBnClickedBtnFile()
{
    CString filename = chooseFile();
    if (filename.IsEmpty())
    {
        return;
    }

    closeFile();

    openFile(filename);

    OnBnClickedBtnPlay();
}

void CDemoH264PlayerDlg::OnBnClickedBtnPlay()
{
    H264_Play(m_handle);
}

void CDemoH264PlayerDlg::OnBnClickedBtnPause()
{
    H264_Pause(m_handle, TRUE);
}

void CDemoH264PlayerDlg::OnBnClickedBtnStop()
{
    H264_Stop(m_handle);
}

void CDemoH264PlayerDlg::OnBnClickedBtnSnap()
{
    char buffer[MAX_PATH] = {0};
    ::GetModuleFileName(NULL, buffer, MAX_PATH);
    CString dir(buffer);

    CTime tmNow = CTime::GetCurrentTime();
    CString filename = tmNow.Format("%Y_%m_%d__%H_%M_%S.bmp");

    filename = dir + "\\" + filename;
    H264_Snap(m_handle, filename);
}

void CDemoH264PlayerDlg::OnBnClickedBtnLive()
{
    if (isPlaying())
    {
        closeLive();
    }

    openLive();

    OnBnClickedBtnPlay();
}


void MyH264DrawCallback(HANDLE handle, HDC hdc, void* pUser)
{
    double pos = H264_GetPos(handle);
    CString text;
    text.Format("pos: %f", pos);

    CRect rc(0, 0, 200, 40);
    CDemoH264PlayerDlg* pDlg = (CDemoH264PlayerDlg*)pUser;
    CWnd* pWnd = pDlg->GetDlgItem(IDC_VIDEO_FRAME);
    if (pWnd)
    {
        pWnd->GetClientRect(&rc);
    }
    
    //SetBkMode(hdc, TRANSPARENT);
    ::DrawText(hdc, text, text.GetLength(), &rc, DT_CENTER|DT_SINGLELINE);
}


void AppVideoCallBack(
                          long nPort,
                          char *pYUVBuf,
                          long nSize,
                          long nWidth,
                          long nHeight,
                          long nStamp,
                          long nUser)
{
    TRACE("VideoCallBack. width: %d, height:%d\n", nWidth, nHeight);
}


bool CDemoH264PlayerDlg::openLive()
{
    TCHAR szFilters[]= _T("H264 Files (*.h264)|*.h264|PS Files (*.vob)|*.vob|All Files (*.*)|*.*||");

    CFileDialog fileDlg(TRUE, _T("any"), _T("*.*"),
        OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);
    if (IDOK != fileDlg.DoModal())
    {
        return false;
    }

    CString filename = fileDlg.GetPathName();

    m_pFile = fopen(filename.GetString(), "rb");
    if (!m_pFile)
    {
        return false;
    }

    CWnd* pWnd = GetDlgItem(IDC_VIDEO_FRAME);

    m_handle = H264_CreatePlayer(kElementStream); 
    H264_SetVideoWnd(m_handle, pWnd->m_hWnd);
    H264_Play(m_handle);
    H264_SetDrawCallback(m_handle, MyH264DrawCallback, this);

    SetTimer(100, 100, NULL);

    return true;
}



void CDemoH264PlayerDlg::closeLive()
{
    closePlayer();
}

bool CDemoH264PlayerDlg::isPlaying()
{
    return (m_handle != NULL);
}

bool CDemoH264PlayerDlg::openFile(const CString& filename)
{
    m_handle = H264_OpenFile(filename.GetString());
    if (m_handle != NULL)
    {
        CWnd* pWnd = GetDlgItem(IDC_VIDEO_FRAME);
        H264_SetVideoWnd(m_handle, pWnd->m_hWnd);
        H264_Play(m_handle);
        H264_SetDrawCallback(m_handle, MyH264DrawCallback, this);
        H264_SetFileEndMsg(m_handle, m_hWnd, WM_FILE_END);
        m_isFilePlayer = true;
    }
    
    return (m_handle != NULL);
}

void CDemoH264PlayerDlg::closeFile()
{
    closePlayer();
}

CString CDemoH264PlayerDlg::chooseFile()
{
    TCHAR szFilters[]= _T("H264 Files (*.h264)|*.h264|TS files (*.ts)|*.ts|mp4 files (*.mp4)|*.mp4|All Files (*.*)|*.*||");

    CFileDialog fileDlg(TRUE, _T("h264"), _T("*.h264"),
        OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);
    if (IDOK != fileDlg.DoModal())
    {
        return "";
    }

    return fileDlg.GetPathName();
}


void CDemoH264PlayerDlg::closePlayer()
{
    if (m_handle == NULL)
    {
        return;
    }

    if (m_isFilePlayer)
    {
        H264_CloseFile(m_handle);
        m_handle = NULL;
    }
    else
    {
        H264_DestroyPlayer(m_handle);
        m_handle = NULL;
    }

    KillTimer(100);

    if (m_pFile)
    {
        fclose(m_pFile);
        m_pFile = NULL;
    }
}


void CDemoH264PlayerDlg::OnDestroy()
{
    CDialog::OnDestroy();

    uninit();
}

void CDemoH264PlayerDlg::OnTimer(UINT_PTR nIDEvent)
{
    
    if (m_pFile)
    {
        const size_t   BUFFER_SIZE = 1024 * 10;
        BYTE buffer[BUFFER_SIZE + 1] = {0};

        int count = fread(buffer, 1, BUFFER_SIZE, m_pFile);
        if (count > 0)
        {
            H264_InputData(m_handle, buffer, count);
        }
        else
        {
            KillTimer(nIDEvent);
        }
    }
    else
    {
        KillTimer(nIDEvent);
    }


    CDialog::OnTimer(nIDEvent);
}

LRESULT CDemoH264PlayerDlg::OnFileEndMsg(WPARAM w, LPARAM l)
{
    OnBnClickedBtnStop();
    
    TRACE("File End.\n");

    return 0;
}