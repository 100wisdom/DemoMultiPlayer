
// DemoH264PlayerDlg.h : 头文件
//

#pragma once

#include "H264Player.h"

// CDemoH264PlayerDlg 对话框
class CDemoH264PlayerDlg : public CDialog
{
// 构造
public:
	CDemoH264PlayerDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DemoH264Player_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()



protected:
    void init();
    void uninit();

    void layout();
    void layoutBtn(const CRect& rcBar);


    bool openLive();
    void closeLive();

    bool isPlaying();

    bool openFile(const CString& filename);
    void closeFile();

    CString chooseFile();
    
    void closePlayer();

    HANDLE  m_handle;
    bool    m_isFilePlayer;

    FILE*   m_pFile;

public:
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnBnClickedBtnFile();
    afx_msg void OnBnClickedBtnPlay();
    afx_msg void OnBnClickedBtnPause();
    afx_msg void OnBnClickedBtnStop();
    afx_msg void OnBnClickedBtnSnap();
    afx_msg void OnBnClickedBtnLive();
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT_PTR nIDEvent);

    afx_msg LRESULT OnFileEndMsg(WPARAM w, LPARAM l);

};
