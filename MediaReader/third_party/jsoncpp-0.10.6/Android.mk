LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := jsoncpp
LOCAL_MODULE_FILENAME := libjsoncpp

LOCAL_CPPFLAGS += -fexceptions -frtti -DXLOCALE_NOT_USED=1

LOCAL_C_INCLUDES := $(LOCAL_PATH)/ \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/src \
	

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

SRC_FILES := $(wildcard \
		$(LOCAL_PATH)/src/*.c \
		$(LOCAL_PATH)/src/lib_json/*.cpp \
		)
		
SRC_FILES := $(SRC_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES += $(SRC_FILES)

#LOCAL_CFLAGS += -D

include $(BUILD_STATIC_LIBRARY)

