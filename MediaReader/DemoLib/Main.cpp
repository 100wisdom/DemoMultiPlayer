/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-04-06
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include "BasicType.h"
#include "LibMediaReader.h"
#include "include/MediaEvent.h"
#include "SharedPtr.h"
#include "TFileUtil.h"
#include "TStringCast.h"
#include <assert.h>
#include <errno.h>
#include "TEvent.h"
#include "H264Player.h"


#pragma comment(lib,"H264Player.lib")


#ifdef WIN32
	#include "VideoWindow.h"
#else
	#include <unistd.h>
#endif //


/// util
void allocMediaFormat(MFormat& fmt)
{
	fmt.vPropSize = 1024 * 4;
	fmt.vProp = new unsigned char[fmt.vPropSize];

	fmt.configSize = 1024 * 4;
	fmt.config = new unsigned char[fmt.configSize];
}

void freeMediaFormat(MFormat& fmt)
{
	if (fmt.vProp)
	{
		free(fmt.vProp);
		fmt.vProp = 0;
	}

	fmt.vPropSize = 0;

	if (fmt.config)
	{
		free(fmt.config);
		fmt.config = 0;
	}

	fmt.configSize = 0;
}

class AutoMFormat : public MFormat
{
public:
	AutoMFormat()
	{
		MFormat& fmt = *this;
		memset(&fmt, 0, sizeof(fmt));

		allocMediaFormat(fmt);
	}

	~AutoMFormat()
	{
		freeMediaFormat(*this);
	}

};

class H264Player
{
public:
	virtual bool open()
	{
		return true;
	}

	virtual void close()
	{

	}

	virtual void input(MPacket& pkt)
	{

	}

};


class WinH264Player : public H264Player
{
public:
	WinH264Player()
	{
	}

	bool open()
	{
		m_window.open("CVideoWindow");
		m_window.setReadyCallback(windowReadyCallback, this);

		H264_Init();

		m_handle = H264_CreatePlayer(kElementStream);
		if (!m_handle)
		{
			return false;
		}

		return true;
	}

	void close()
	{
		if (m_handle)
		{
			H264_Stop(m_handle);
			H264_DestroyPlayer(m_handle);
			m_handle = NULL;
		}

		H264_Uninit();

		m_window.close();
	}

	void input(MPacket& pkt)
	{
		if (!m_handle)
		{
			return;
		}

		H264_InputData(m_handle, pkt.data, pkt.size);
	}

	static void windowReadyCallback(HWND hwnd, void* user)
	{
		WinH264Player* pthis = (WinH264Player*)user;
		pthis->onWindowReady();
	}

	void onWindowReady()
	{
		HWND hwnd = m_window.getHwnd(); // GetConsoleWindow();
		H264_SetVideoWnd(m_handle, hwnd);

		H264_Play(m_handle);
	}

	HANDLE m_handle;
	VideoWindow m_window;

};


comn::Event g_event;

void msleep(int milliseconds)
{
	g_event.timedwait(milliseconds);
}

bool g_formatReady = false;


struct CasterGuard
{
public:
    CasterGuard()
    {
        mreader_init();
    }

    ~CasterGuard()
    {
        mreader_quit();
    }
};

void  DemoMReaderEventCallback(void* reader, int event, int64_t value, void* context)
{
	printf("-------- event:%d\n", event);

	if (event == av::MEDIA_EVENT_FORMAT_READY)
	{
		/// 流已经打开, 可以获取媒体格式等信息了

		mreader_t handle = *(mreader_t*)context;
		MFormat fmt;
		memset(&fmt, 0, sizeof(fmt));
		allocMediaFormat(fmt);

		mreader_getFormat(handle, &fmt);

		std::string hex = comn::StringCast::toHexGroupString(fmt.vProp, fmt.vPropSize);
		printf("-------- hex:%s\n", hex.c_str());

		freeMediaFormat(fmt);

		g_formatReady = true;
		g_event.post();
	}
	else if (event == av::MEDIA_EVENT_OPEN_FAILED)
	{
		g_event.post();

		printf("failed to open stream.\n");
		/// 打开流失败, 处理定时重连
	}
	else if (event == av::MEDIA_EVENT_END)
	{
		printf("stream is end\n"); /// 流结束. 因为网络中断或者回放结尾
		/// 在此处理重连
	}
}

bool waitForMediaFormat(int ms)
{
	return g_event.timedwait(ms) && g_formatReady;
}

/// rtsp://192.168.3.53/me.mkv
/// http://192.168.3.53:9000/media/me.mp4
/// rtsp://admin:sjld16301@192.168.3.65/
/// rtsp://192.168.3.22/0
/// mtcp://192.168.3.1:2554/sub

int main(int argc, char** argv)
{
    CasterGuard casterGuard;

	std::shared_ptr< H264Player > player;

#ifdef WIN32
	player.reset(new WinH264Player());
#else
	player.reset(new H264Player());
#endif //

	player->open();

	const char* url = argv[1];
	const char* recName = "demo.mp4";
	const char* dumpName = "dump.h264";

	FILE* file = fopen(dumpName, "wb");

	AutoMFormat fmt;

	mreader_enableLog("demo.log");
    
    mreader_t handle = 0;
	// rtsp_transport=tcp 表示使用tcp传输协议
	int ret = mreader_open(&handle, url, "probesize=80240;stimeout=5000000");
	if (ret != 0)
	{
		return -1;
	}

	mreader_setEventCallback(handle, DemoMReaderEventCallback, &handle);

	ret = mreader_play(handle);

	int duration = -1;
	ret = mreader_getDuration(handle, &duration);

	int seekable = mreader_seekable(handle);
	int isLive = mreader_isLive(handle);

	//ret = mreader_startRecord(handle, recName);

	ret = mreader_getFormat(handle, &fmt);

	if (fmt.codec > 0 || fmt.audioCodec > 0)
	{
		g_formatReady = true;
		g_event.post();
	}

	/// 等待媒体就绪
	if (!waitForMediaFormat(1000 * 20))
	{
		mreader_close(handle);

		if (file)
		{
			fclose(file);
		}

		return -1;
	}


	/// 获取媒体格式
	
	if (file && fmt.vPropSize > 0)
	{
		fwrite(fmt.vProp, 1, fmt.vPropSize, file);

		MPacket pkt;
		memset(&pkt, 0, sizeof(pkt));
		pkt.data = fmt.vProp;
		pkt.size = fmt.vPropSize;
		player->input(pkt);
	}

	static const  int MAX_BUFFER = 1024 * 1024;
	uint8_t* buffer = new uint8_t[MAX_BUFFER];

	int count = 100000;
	while (count > 0)
	{
		count --;

		MPacket pkt;
		memset(&pkt, 0, sizeof(pkt));
		pkt.data = buffer;
		pkt.size = MAX_BUFFER;

		ret = mreader_read(handle, &pkt);
		//printf("mreader_read return: %d. %d\n", ret, EAGAIN);
		if (ret == 0)
		{
			if (pkt.type == MTYPE_VIDEO)
			{
				std::string hex = comn::StringCast::toHexGroupString(pkt.data, 16);
				//printf("video data. size:%d\t %s\n", pkt.size, hex.c_str());

				if (file)
				{
					fwrite(pkt.data, 1, pkt.size, file);
				}

				player->input(pkt);
			}
			else if (pkt.type == MTYPE_AUDIO)
			{
				printf("pkt. type:%d, size:%d, flags:%d\n", pkt.type, pkt.size, pkt.flags);

				std::string hex = comn::StringCast::toHexGroupString(pkt.data, 16);
				printf("audio pkt data. %s\n", hex.c_str());
			}
		}
		else if (ret == EAGAIN)
		{
			msleep(50);
		}
		else
		{
			break;
		}
	}

	delete[] buffer;

    mreader_close(handle);

	if (file)
	{
		fclose(file);
	}

	return 0;
}




