/*
 * VideoWindow.h
 *
 *  Created on: 2017年4月11日
 *      Author: chuanjiang.zh
 */

#ifndef VIDEOWINDOW_H_
#define VIDEOWINDOW_H_

#include "BasicType.h"
#include <string>
#include "TThread.h"


typedef void (*WindowReadyCallback)(HWND hwnd, void* user);


class VideoWindow : public comn::Thread
{
public:
	VideoWindow();
	virtual ~VideoWindow();

	bool open(const char* title);

	void close();

	bool isValidWindow();

	HWND getHwnd();

	void setReadyCallback(WindowReadyCallback cb, void* user);

protected:
	virtual int run();
	virtual void doStop();

	virtual bool startup();
	virtual void cleanup();

	bool createWindow();
	void destroyWindow();

protected:
	HWND	m_hwnd;
	std::string	m_title;

	WindowReadyCallback m_readyCallback;
	void*	m_user;

};

#endif /* VIDEOWINDOW_H_ */
