/*
 * IniProperties.cpp
 *
 *  Created on: 2015年12月22日
 *      Author: terry
 */

#include "IniProperties.h"
#include "IniFile.h"
 

namespace comn
{

IniProperties::IniProperties():
		m_iniFile(new IniFile())
{
}

IniProperties::~IniProperties()
{
}

bool IniProperties::load(const char* filename)
{
	m_filename = filename;
	return m_iniFile->load(filename);
}

bool IniProperties::store(const char* filename)
{
	if (filename == NULL)
	{
		filename = m_filename.c_str();
	}
	return m_iniFile->save(filename);
}

std::string IniProperties::getFilename()
{
	return m_filename;
}

bool IniProperties::get(const std::string& path, std::string& value)
{
    std::string sec;
    std::string key;
    split(path, sec, key);
    return m_iniFile->queryValue(sec.c_str(), key.c_str(), value);
}

bool IniProperties::set(const std::string& path, const std::string& value)
{
    std::string sec;
    std::string key;
    split(path, sec, key);

    m_iniFile->setValue(sec.c_str(), key.c_str(), value);
    return true;
}

bool IniProperties::split(const std::string& path, std::string& sec, std::string& name)
{
	if (path.empty())
	{
		return false;
	}

	sec = "App";
	name = path;

	size_t pos = path.find('.');
	if (pos != std::string::npos)
	{
		sec = path.substr(0, pos);
		name = path.substr(pos + 1);
	}
	return true;
}

size_t IniProperties::sections(StringArray& arr)
{
    size_t count = m_iniFile->getSectionCount();
    for (size_t i = 0; i < count; i ++)
    {
        IniFile::IniSection& section = m_iniFile->getSection(i);
        arr.push_back(section.name);
    }
    return count;
}

size_t IniProperties::keys(const std::string& section, StringArray& arr)
{
    if (!m_iniFile->existSection(section.c_str()))
    {
        return 0;
    }
    IniFile::IniSection& sec = m_iniFile->ensure(section.c_str());
    for (size_t i = 0; i < sec.entryArray.elements.size(); i ++)
    {
        arr.push_back(sec.entryArray.elements[i].key);
    }
    return sec.entryArray.elements.size();
}

bool IniProperties::exist(const std::string& path)
{
    std::string sec;
    std::string key;
    split(path, sec, key);
    if (!m_iniFile->existSection(sec.c_str()))
    {
        return false;
    }
    return m_iniFile->ensure(sec.c_str()).exist(key.c_str());
}

} /* namespace util */
