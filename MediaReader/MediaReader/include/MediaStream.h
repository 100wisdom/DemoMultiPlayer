/*
 * MediaStream.h
 *
 *  Created on: 2016年3月17日
 *      Author: terry
 */

#ifndef MEDIASTREAM_H_
#define MEDIASTREAM_H_


#include "MediaFormat.h"
#include <string>
#include "Ffmpeg.h"
#include "SharedPtr.h"



namespace av
{


typedef std::shared_ptr< AVFrame >		AVFramePtr;

typedef std::shared_ptr< AVPacket >		AVPacketPtr;



}


#endif /* MEDIASTREAM_H_ */
