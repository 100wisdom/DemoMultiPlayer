/*
 * MediaConst.h
 *
 *  Created on: 2016年6月11日
 *      Author: terry
 */

#ifndef CORE_MEDIACONST_H_
#define CORE_MEDIACONST_H_

#include "BasicType.h"


namespace av
{

static const int64_t MAX_CACHE_DURATION = 1000;
static const int64_t TUNE_CACHE_DURATION = 400;
static const int64_t MIN_CACHE_DURATION = 100;

static const size_t MAX_QUEUE_SIZE = 300;



} /* namespace av */

#endif /* CORE_MEDIACONST_H_ */
