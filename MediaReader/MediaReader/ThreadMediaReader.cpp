/*
 * ThreadMediaReader.cpp
 *
 *  Created on: 2016��8��13��
 *      Author: zhengboyuan
 */

#include "ThreadMediaReader.h"
#include "CMediaSourceFactory.h"
#include "FfmpegUtil.h"
#include "CFileMediaSink.h"
#include "CLog.h"
#include "MediaEvent.h"
#include "Utility.h"
#include "TimeHelper.h"
#include <errno.h>
#include "MediaConst.h"


namespace av
{


static const int MAX_SYNC_TIME = 2 * 1000 * 1000;
static const AVRational PREREAD_SCALE = av_make_q(5,2);


ThreadMediaReader::ThreadMediaReader():
		m_isLive(true),
		m_state(STATE_STOPPED),
		m_curTime(),
		m_scale(av_make_q(1,1)),
		m_endOfStream(),
		m_eventCallback(),
		m_eventContext()
{
	m_factory.reset(new CMediaSourceFactory());
}

ThreadMediaReader::~ThreadMediaReader()
{
	close();
}

int ThreadMediaReader::open(const std::string& url, const std::string& params)
{
	if (url.empty())
	{
		return EINVAL;
	}

	if (isOpen())
	{
		close();
	}

	m_url = url;
	m_params = params;

	m_endOfStream = false;

	assert(isRunning() == false);
	util::CommandThread::start();

	postCommand(kOpen);

	return 0;
}

void ThreadMediaReader::close()
{
	int state = getState();
	if (state != STATE_STOPPED)
	{
		stop();
	}

	if (isOpen())
	{
		postCommand(kClose);
	}

	util::CommandThread::stop();

	m_url.clear();

	m_endOfStream = false;
	m_isLive = true;
}

bool ThreadMediaReader::isOpen()
{
	return isRunning();
}

int ThreadMediaReader::getFormat(MediaFormat& fmt)
{
	if (!m_source)
	{
		return EAGAIN;
	}

	int ret = 0;
	comn::AutoCritSec lock(m_cs);
	fmt = m_format;
	if (!m_format.isValid())
	{
		ret = m_endOfStream ? EIO : EAGAIN;
	}
	return ret;
}

int ThreadMediaReader::getDuration()
{
	return m_format.m_duration;
}

int ThreadMediaReader::play()
{
	postCommand(kPlay);
	return 0;
}

int ThreadMediaReader::pause()
{
	postCommand(kPause);
	return 0;
}

void ThreadMediaReader::stop()
{
	postCommand(kStop);
}

int ThreadMediaReader::getState()
{
	return m_state;
}

bool ThreadMediaReader::seekable()
{
	return m_source && m_source->seekable();
}

int ThreadMediaReader::seek(int64_t offset)
{
	if (offset < 0)
	{
		return EINVAL;
	}

	{
		comn::AutoCritSec lock(m_cs);
		m_curTime = offset;
	}

	postCommand(kSeek, offset);
	return 0;
}

int64_t ThreadMediaReader::getTime()
{
	return m_curTime / 1000;
}

int ThreadMediaReader::setScale(float scale)
{
	if ((scale < MIN_SCALE) || (scale > MAX_SCALE))
	{
		return EINVAL;
	}

	AVRational r = av_d2q(scale, MAX_SCALE);

	postCommand(kSetScale, r.num, r.den);

	return 0;
}

float ThreadMediaReader::getScale()
{
	return (float)av_q2d(m_scale);
}

int ThreadMediaReader::read(AVPacketPtr& pkt)
{
	pkt = m_pktQueue.pop();
	if (!pkt)
	{
		return m_endOfStream ? EIO : EAGAIN;
	}
	return 0;
}

void ThreadMediaReader::interrupt()
{
}

bool ThreadMediaReader::isLive()
{
	return m_source && m_source->isLive();
}


int ThreadMediaReader::startRecord(const std::string& filename)
{
	if (filename.empty())
	{
		return EINVAL;
	}

	postCommand(kStartRecord, filename);
	return 0;
}

void ThreadMediaReader::stopRecord()
{
	postCommand(kStopRecord);
}

bool ThreadMediaReader::isRecording()
{
	comn::AutoCritSec lock(m_cs);
	return (m_fileSink.get() != NULL);
}


void ThreadMediaReader::onCommand(Command& cmd)
{
	if (cmd.type == kOpen)
	{
		handleOpen();
	}
	else if (cmd.type == kClose)
	{
		handleClose();
	}
	else if (cmd.type == kPlay)
	{
		handlePlay();
	}
	else if (cmd.type == kPause)
	{
		handlePause();
	}
	else if (cmd.type == kStop)
	{
		handleStop();
	}
	else if (cmd.type == kSeek)
	{
		int64_t offset = cmd.at(0).toLong();
		handleSeek(offset);
	}
	else if (cmd.type == kSetScale)
	{
        AVRational scale;
        scale.num = cmd.at(0).toInt();
        scale.den = cmd.at(1).toInt();

		handleSetScale(scale);

		fireMediaEvent(MEDIA_EVENT_SCALE, 0);
	}
	else if (cmd.type == kSnap)
	{
		std::string filePath = cmd.at(0).toString();
		handleSnap(filePath);
	}
	else if (cmd.type == kFlush)
	{
		handleFlush();
	}
	else if (cmd.type == kSetProp)
	{
		std::string key = cmd.at(0).toString();
		std::string value = cmd.at(1).toString();
		handleSetProp(key, value);
	}
	else if (cmd.type == kStartRecord)
	{
		std::string filePath = cmd.at(0).toString();
		handleStartRecord(filePath);
	}
	else if (cmd.type == kStopRecord)
	{
		handleStopRecord();
	}
	else if (cmd.type == kStartRender)
	{
		handleStartRender();
	}
	else if (cmd.type == kStopRender)
	{
		handleStopRender();
	}
	else if (cmd.type == kSetVideoWindow)
	{
		void* window = cmd.at(0).toPtr();
		handleSetVideoWindow(window);
	}
	else
	{

	}

}

void ThreadMediaReader::onIdle()
{
	if (!m_source)
	{
		timedwait(-1);
		return;
	}

	if (m_state != STATE_PLAYING)
	{
		timedwait(-1);
		return;
	}

	AVPacketPtr pkt(new AVPacket(), ffmpeg::PacketDeleter());
	av_init_packet(pkt.get());

	int rc = m_source->read(*pkt.get());
	if (rc == 0)
	{
		if (!m_isLive)
		{
			int delay = computeDelay(pkt);
			if (delay > 0)
			{
				timedwait(delay);
			}
		}

		dispatchPacket(pkt);
	}
	else if (rc == AVERROR_EOF)
	{
		fireMediaEvent(MEDIA_EVENT_END, 0);
		timedwait(1000 * 10);
	}
	else if (rc == AVERROR(EAGAIN))
	{
		// continue;
		timedwait(50);
	}
	else
	{
		char errbuf[AV_ERROR_MAX_STRING_SIZE] = {0};
		av_make_error_string(errbuf, AV_ERROR_MAX_STRING_SIZE, rc);

		CLog::info("ThreadMediaReader read error! code:%08x, err: %s, url:%s\n",
			rc, errbuf,
			m_url.c_str());

		fireMediaEvent(MEDIA_EVENT_END, 0);
		timedwait(1000 * 10);
	}
}

bool ThreadMediaReader::startup()
{
	return true;
}

void ThreadMediaReader::cleanup()
{
	while (fetchAndHandle())
	{
		// pass
	}
}

void ThreadMediaReader::doStop()
{
    util::CommandThread::doStop();

    if (m_source)
    {
        m_source->interrupt();
    }
}


void ThreadMediaReader::handleOpen()
{
	m_source.reset(m_factory->create(m_url, m_params), MediaSourceDeleter(m_factory));
	if (!m_source)
	{
        CLog::error("source is null.\n");
        return;
    }

    fireMediaEvent(MEDIA_EVENT_OPENING, 0);

	int rc = m_source->open(m_url, m_params);
	if (rc != 0)
	{
		fireMediaEvent(MEDIA_EVENT_OPEN_FAILED, 0);
		m_source.reset();
		return;
	}

    fireMediaEvent(MEDIA_EVENT_OPENED, 0);

	m_source->getFormat(m_format);
	m_isLive = m_source->isLive();

	fireMediaEvent(MEDIA_EVENT_FORMAT_READY, 0);
    fireMediaFormat(m_format);

}

void ThreadMediaReader::handleClose()
{
	doCloseFile();
}

void ThreadMediaReader::handlePlay()
{
    if (!m_source)
    {
        return;
    }

    m_source->play();

	setState(STATE_PLAYING);

	fireMediaEvent(MEDIA_EVENT_PLAYING, 0);
}

void ThreadMediaReader::handlePause()
{
    if (!m_source)
    {
        return;
    }

	setState(STATE_PAUSED);

	m_source->pause();

	fireMediaEvent(MEDIA_EVENT_PAUSED, 0);
}

void ThreadMediaReader::handleStop()
{
	setState(STATE_STOPPED);

    if (m_source)
    {
	    m_source->stop();
    }

	fireMediaEvent(MEDIA_EVENT_STOPPED, 0);

	/// ֹͣ¼��
    if (m_fileSink)
    {
        m_fileSink->close();
	    m_fileSink.reset();
    }

	m_scale = av_make_q(1,1);
    m_curTime = 0;
}

void ThreadMediaReader::handleSeek(int64_t offset)
{
	fireMediaEvent(MEDIA_EVENT_SEEKING, 0);

	int rc = m_source->seek(offset);
	if (rc != 0)
	{
		CLog::warning("failed to seek. offset:%d", (int)offset);
	}

	fireMediaEvent(MEDIA_EVENT_SEEKED, 0);

	handleFlush();

}

void ThreadMediaReader::handleSetScale(AVRational scale)
{
	m_scale = scale;
}

void ThreadMediaReader::handleSnap(const std::string& filePath)
{
}

void ThreadMediaReader::handleFlush()
{
	m_pktQueue.clear();
}

void ThreadMediaReader::handleStartRecord(const std::string& filePath)
{
	if (!m_fileSink)
	{
		m_fileSink.reset(new CFileMediaSink());
        m_fileSink->open(filePath.c_str());

		if (m_format.isValid())
		{
			m_fileSink->onMediaFormat(m_format);
		}
	}
    else
    {
        m_fileSink->setFile(filePath.c_str());
    }
}

void ThreadMediaReader::handleStopRecord()
{
	m_fileSink.reset();
}

void ThreadMediaReader::handleSetProp(std::string& key, std::string& value)
{
//	std::string sec;
//	std::string subName;
//
//	comn::StringUtil::split(key, comn::IniProperties::SEPERATOR, sec, subName);
}

void ThreadMediaReader::handleStartRender()
{

}

void ThreadMediaReader::handleStopRender()
{

}

void ThreadMediaReader::handleSetVideoWindow(void* window)
{

}


void ThreadMediaReader::fireMediaEvent(int type, int64_t value)
{
	//if (type != MEDIA_EVENT_PACKET)
    CLog::info("media event. type:%d. name:%s\n", type, Utility::getEventName(type));

	{
		comn::AutoCritSec lock(m_cs);
		if (m_eventCallback)
		{
			(*m_eventCallback)(this, type, value, m_eventContext);
		}
	}

	if (type == STREAM_EVENT_END)
	{
		comn::AutoCritSec lock(m_cs);
		if (m_fileSink)
		{
			m_fileSink->onMediaEvent(type);
		}

		m_endOfStream = true;
	}
	else if (type == MEDIA_EVENT_OPEN_FAILED)
	{
		comn::AutoCritSec lock(m_cs);
		m_endOfStream = true;
	}

}


void ThreadMediaReader::fireMediaFormat(const MediaFormat& fmt)
{
	comn::AutoCritSec lock(m_cs);
	if (m_fileSink)
	{
		m_fileSink->onMediaFormat(fmt);
	}

}

void ThreadMediaReader::fireMediaPacket(AVPacketPtr& pkt)
{
	int64_t dist = pkt->pts - m_curTime;

	m_curTime = pkt->pts;

	if (dist/1000 > 500)
	{
		const char* name = (pkt->stream_index == 0) ? "video" : "audio";
		CLog::debug("ThreadMediaReader(%p) packet(%s). duration:%d, dts:%I64d, pts:%I64d, dist:%d.\n",
				this, name, (int)pkt->duration/1000, pkt->dts/1000, pkt->dts/1000, (int)(dist/1000));
	}

	//fireMediaEvent(MEDIA_EVENT_PACKET, m_pktQueue.size());

	/// ������ʾ��ʱ
	if ((pkt->stream_index == 0) &&
		((pkt->flags & AV_PKT_FLAG_KEY) != 0))
	{
		//size_t dropped = m_pktQueue.dropUntilKeyFrame();
		if (m_pktQueue.size() > MAX_QUEUE_SIZE)
		{
			size_t dropped = m_pktQueue.reserveFront(2);
			if (dropped > 0)
			{
				CLog::info("ThreadMediaReader queue:%d, dropped:%d\n", m_pktQueue.size(), dropped);
			}
		}

		m_pktQueue.push(pkt);
	}
	else
	{
		m_pktQueue.push(pkt);
	}

	comn::AutoCritSec lock(m_cs);
	if (m_fileSink)
	{
		m_fileSink->onMediaPacket(pkt);
	}

}


void ThreadMediaReader::doCloseFile()
{
	if (m_source)
	{
		m_source->close();
        m_source.reset();

		fireMediaEvent(MEDIA_EVENT_CLOSED, 0);
	}

	m_isLive = false;

}

void ThreadMediaReader::setState(StreamState state)
{
	comn::AutoCritSec lock(m_cs);
	m_state = state;
}

void ThreadMediaReader::dispatchPacket(AVPacketPtr& pkt)
{
	fireMediaPacket(pkt);
}


int ThreadMediaReader::computeDelay(AVPacketPtr& pkt)
{
	int64_t delay = 0;
	int64_t curClock = getClock();
	TimePoint curPoint(curClock, pkt->pts);
	TimePoint& lastPoint = m_lastPoint[pkt->stream_index];

	if (pkt->stream_index != 0)
	{
		delay = 0;
	}

	if (!lastPoint.isSet())
	{
		lastPoint = curPoint;
	}
	else
	{
		int64_t ptsDelta = curPoint.m_pts - lastPoint.m_pts;
		int64_t clockDelta = curPoint.m_clock - lastPoint.m_clock;
		if (ptsDelta > MAX_SYNC_TIME)
		{
			lastPoint.reset();
		}
		else if (ptsDelta < 0)
		{
			lastPoint.reset();
		}
		else
		{
            int64_t pretime = av_rescale_q(ptsDelta, PREREAD_SCALE, m_scale);
			delay = (ptsDelta - clockDelta) - pretime;

			lastPoint.reset(lastPoint.m_clock + ptsDelta, curPoint.m_pts);
		}
	}

	//const char* name = (pkt->stream_index == 0) ? "video" : "audio";
	//CLog::debug("computeDelay. packet(%s) clock:%I64d, pts:%I64d, delay:%d\n", name, curPoint.m_clock/1000, curPoint.m_pts/1000, delay/1000);

	return (int)delay/1000;
}

int64_t ThreadMediaReader::getClock()
{
    int64_t clk = util::TimeHelper::getClock();
    if ((m_scale.num > 0) && (m_scale.num != m_scale.den))
    {
        clk = av_rescale(clk, m_scale.num, m_scale.den);
    }
    return clk;
}


void ThreadMediaReader::setEventCallback(ReaderEventCallback cb, void* context)
{
	CLog::info("setEventCallback %p,%p\n", cb, context);

	comn::AutoCritSec lock(m_cs);
	m_eventCallback = cb;
	m_eventContext = context;
}




} /* namespace av */
