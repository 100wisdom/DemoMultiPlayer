/*
 * MediaTransportHelper.h
 *
 *  Created on: 2016年6月23日
 *      Author: terry
 */

#ifndef MEDIATRANSPORTHELPER_H_
#define MEDIATRANSPORTHELPER_H_

#include <json/json.h>
#include "MediaFormat.h"


namespace av
{


class MediaTransportHelper
{
public:
	MediaTransportHelper();
	virtual ~MediaTransportHelper();

    static void toJson(const MediaFormat& fmt, Json::Value& value);

    static bool fromJson(MediaFormat& fmt, Json::Value& value);

    static void makeError(Json::Value& value, int err, const char* desc);

    
};



} /* namespace av */

#endif /* MEDIATRANSPORTHELPER_H_ */
