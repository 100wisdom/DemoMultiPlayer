/*
 * Utility.cpp
 *
 *  Created on: 2016年5月11日
 *      Author: terry
 */

#include "Utility.h"
#include "MediaEvent.h"


namespace av
{

Utility::Utility()
{
}

Utility::~Utility()
{
}

size_t Utility::copyProp(comn::IniProperties& props, const std::string& secName, util::ParamSet& params)
{
	comn::IniProperties::StringArray keys;
	size_t count = props.keys(secName, keys);
	for (size_t i = 0; i < keys.size(); i ++)
	{
		std::string path = secName + comn::IniProperties::SEPERATOR + keys[i];
		std::string value;
		if (props.get(path, value))
		{
			params.set(path, value);
		}
	}
	return count;
}

size_t Utility::copyProp(comn::IniProperties& props, util::ParamSet& params)
{
	size_t count = 0;
	comn::IniProperties::StringArray secs;
	props.sections(secs);
	for (size_t i = 0; i < secs.size(); i ++)
	{
		count += copyProp(props, secs[i], params);
	}
	return count;
}


struct EventNamePair
{
    int event;
    const char* name;
};

static EventNamePair s_eventNames[] = 
{
    {MEDIA_EVENT_OPENING, "opening"},
	{MEDIA_EVENT_OPEN_FAILED, "failed"},
	{MEDIA_EVENT_OPENED, "opened"},
	{MEDIA_EVENT_CLOSED, "closed"},
	{MEDIA_EVENT_END, "end"},
	{MEDIA_EVENT_FORMAT_READY, "format ready"},
	{MEDIA_EVENT_SEEKING, "seeking"},
	{MEDIA_EVENT_SEEKED, "seeked"},

	{MEDIA_EVENT_FULLSCREEN, "fullscreen"},
	{MEDIA_EVENT_VOLUME, "volume"},
	{MEDIA_EVENT_MUTE, "mute"},
	{MEDIA_EVENT_SCALE, "scale"},

	{MEDIA_EVENT_PLAYING, "playing"},
	{MEDIA_EVENT_PAUSED, "paused"},
	{MEDIA_EVENT_STOPPED, "stopped"},

	{MEDIA_EVENT_BLACK_START, "black start"},
	{MEDIA_EVENT_BLACK_END, "black end"},

};

const char* Utility::getEventName(int event)
{
    size_t count = sizeof(s_eventNames)/sizeof(s_eventNames[0]);
    for (size_t i = 0; i < count; i ++)
    {
        if (event == s_eventNames[i].event)
        {
            return s_eventNames[i].name;
        }
    }
    return "unknown";
}




} /* namespace av */
