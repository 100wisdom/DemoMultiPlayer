/*
 * AacHelper.cpp
 *
 *  Created on: 2015年12月7日
 *      Author: terry
 */

#include "AacHelper.h"
#include "TStringUtil.h"


namespace av
{

static const int s_freqTable[16] =
{
	96000, 88200, 64000, 48000,
	44100, 32000, 24000, 22050,
	16000, 12000, 11025, 8000,
	7350, 0, 0, 0
};


AacHelper::AacHelper()
{
}

AacHelper::~AacHelper()
{
}

int AacHelper::getFreqIndex(int freq)
{
	int idx = 8;
	size_t count = sizeof(s_freqTable)/sizeof(s_freqTable[0]);
	for (size_t i = 0; i < count; ++ i)
	{
		if (freq == s_freqTable[i])
		{
			idx = i;
			break;
		}
	}
	return idx;
}

bool AacHelper::makeConfig(int profile, int freq, int channels, std::string& config)
{
	int idx = getFreqIndex(freq);
	uint8_t audioSpecificConfig[2];
	uint8_t audioObjectType = profile + 1;
	audioSpecificConfig[0] = (audioObjectType << 3) | (idx >> 1);
	audioSpecificConfig[1] = (idx << 7) | (channels << 3);

	config.assign((char*)audioSpecificConfig, sizeof(audioSpecificConfig));
	return true;
}

std::string AacHelper::makeConfig(int profile, int freq, int channels)
{
	int idx = getFreqIndex(freq);
	uint8_t audioSpecificConfig[2];
	uint8_t audioObjectType = profile + 1;
	audioSpecificConfig[0] = (audioObjectType<<3) | (idx>>1);
	audioSpecificConfig[1] = (idx<<7) | (channels<<3);

	return comn::StringUtil::format("%02X%02x",
			audioSpecificConfig[0], audioSpecificConfig[1]);
}

bool AacHelper::makeAdtsHeader(int profile, int freq, int channels, int packetLen, uint8_t* buf, size_t size)
{
	if (size < ADTS_HEADER_SIZE)
	{
		return false;
	}

	int freqIdx = getFreqIndex(freq);

    // fill in ADTS data
    buf[0] = (uint8_t)0xFF;
    buf[1] = (uint8_t)0xF9;
    buf[2] = (uint8_t)(((profile)<<6) + (freqIdx<<2) +(channels>>2));
    buf[3] = (uint8_t)(((channels&3)<<6) + (packetLen>>11));
    buf[4] = (uint8_t)((packetLen&0x7FF) >> 3);
    buf[5] = (uint8_t)(((packetLen&7)<<5) + 0x1F);
    buf[6] = (uint8_t)0xFC;

	return true;
}

bool AacHelper::isAdts(uint8_t* buf, size_t size)
{
	if (size < ADTS_HEADER_SIZE)
	{
		return false;
	}

	return (buf[0] == 0xff) && ((buf[1] & 0xf0) == 0xf0);
}

bool AacHelper::parseConfig(const uint8_t* buf, size_t size, int& profile, int& freq, int& channels)
{
	if (!buf || size < 2)
	{
		return false;
	}

	uint8_t audioObjectType = buf[0] >> 3;
	profile = audioObjectType - 1;

	int index = ((buf[0] & 0x07) << 1) | (buf[1] >> 7);
	int count = sizeof(s_freqTable) / sizeof(s_freqTable[0]);
	if ((index >= 0) && (index < count))
	{
		freq = s_freqTable[index];
	}
		 
	channels = ((buf[1] & 0x7F) >> 3);

	return true;
}

bool AacHelper::parseAdts(uint8_t* buf, size_t size, int& profile, int& freq, int& channels)
{
	if (!isAdts(buf, size))
	{
		return false;
	}

	profile = (buf[2] & 0xC0) >> 6;

	int freqIdx = (buf[2] & 0x3C) >> 2;
	int count = sizeof(s_freqTable) / sizeof(s_freqTable[0]);
	if ((freqIdx >= 0) && (freqIdx < count))
	{
		freq = s_freqTable[freqIdx];
	}

	channels = (buf[3] >> 6);
	channels += ((buf[2] & 0x01) << 2);

	return true;
}


} /* namespace av */
