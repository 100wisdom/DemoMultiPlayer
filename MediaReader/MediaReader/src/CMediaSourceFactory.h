/*
 * CMediaSourceFactory.h
 *
 *  Created on: 2016年3月18日
 *      Author: terry
 */

#ifndef CMEDIASOURCEFACTORY_H_
#define CMEDIASOURCEFACTORY_H_

#include "MediaSourceFactory.h"

namespace av
{

class CMediaSourceFactory: public MediaSourceFactory
{
public:
	CMediaSourceFactory();
	virtual ~CMediaSourceFactory();

	virtual MediaSource* create(const std::string& url, const std::string& params);

	virtual void destroy(MediaSource* source);

};

} /* namespace av */

#endif /* CMEDIASOURCEFACTORY_H_ */
