/*
 * MediaFileSink.h
 *
 *  Created on: 2016年3月18日
 *      Author: terry
 */

#ifndef MEDIAFILESINK_H_
#define MEDIAFILESINK_H_

#include "MediaPacketSink.h"
#include "MediaStream.h"


namespace av
{

class MediaFileSink: public MediaPacketSink
{
public:
	MediaFileSink();
	virtual ~MediaFileSink();

	explicit MediaFileSink(const std::string& fileName);

};


} /* namespace av */

#endif /* MEDIAFILESINK_H_ */
