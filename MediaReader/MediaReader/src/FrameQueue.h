/*
 * FrameQueue.h
 *
 *  Created on: 2016年3月17日
 *      Author: terry
 */

#ifndef FRAMEQUEUE_H_
#define FRAMEQUEUE_H_

#include "MediaStream.h"
#include "TCriticalSection.h"
#include <deque>


namespace av
{

class FrameQueue
{
public:
	FrameQueue();
	virtual ~FrameQueue();

	size_t push(AVFramePtr& frame);

	AVFramePtr pop();
	size_t size();

	void clear();

	size_t popUntil(int64_t pts);

	size_t popUntilSize(size_t size);

protected:
    typedef std::deque< AVFramePtr >  FrameDeque;

    FrameDeque  m_frames;
    comn::CriticalSection   m_cs;

};


} /* namespace av */

#endif /* FRAMEQUEUE_H_ */
