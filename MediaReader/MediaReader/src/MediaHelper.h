/*
 * MediaHelper.h
 *
 *  Created on: 2016年4月15日
 *      Author: terry
 */

#ifndef CORE_MEDIAHELPER_H_
#define CORE_MEDIAHELPER_H_

#include "MediaStream.h"

namespace av
{

class MediaHelper
{
public:
	MediaHelper();
	virtual ~MediaHelper();

	static bool isVideo(AVFramePtr& frame);
	static bool isVideo(AVFrame* frame);
	static bool isAudio(AVFramePtr& frame);
	static bool isAudio(AVFrame* frame);

    static bool isVideo(AVPacket& pkt);
    static bool isVideo(AVPacket* pkt);

};

} /* namespace av */

#endif /* CORE_MEDIAHELPER_H_ */
