/*
 * Utility.h
 *
 *  Created on: 2016年5月11日
 *      Author: terry
 */

#ifndef SRC_UTILITY_H_
#define SRC_UTILITY_H_

#include "IniProperties.h"
#include "ParamSet.h"


namespace av
{

class Utility
{
public:
	Utility();
	virtual ~Utility();

	static size_t copyProp(comn::IniProperties& props, const std::string& secName, util::ParamSet& params);

	static size_t copyProp(comn::IniProperties& props, util::ParamSet& params);

    static const char* getEventName(int event);

};


} /* namespace av */

#endif /* SRC_UTILITY_H_ */
