/*    file: FrameToBitmap.h
 *    desc:
 *   
 * created: 2014-06-30 20:42:42
 *  author: zhengchuanjiang
 * version: 1.0
 * company: 
 */ 


#if !defined FRAMETOBITMAP_H_
#define FRAMETOBITMAP_H_

#include "Ffmpeg.h"
////////////////////////////////////////////////////////////////////////////
bool frameToBitmap(AVFrame* frame, const char* filename);

////////////////////////////////////////////////////////////////////////////
#endif //FRAMETOBITMAP_H_

