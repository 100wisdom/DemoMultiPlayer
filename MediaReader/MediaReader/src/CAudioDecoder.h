/*
 * CAudioDecoder.h
 *
 *  Created on: 2016年4月14日
 *      Author: terry
 */

#ifndef CAUDIODECODER_H_
#define CAUDIODECODER_H_

#include "AudioDecoder.h"

namespace av
{

class CAudioDecoder: public AudioDecoder
{
public:
	explicit CAudioDecoder(MediaDecoderSink* sink);
	virtual ~CAudioDecoder();

    virtual int open(const MediaFormat& fmt);

    virtual void close();

    virtual bool isOpen();

    virtual int input(AVPacketPtr pkt);

    virtual void setSink(MediaDecoderSink* sink);

    virtual void flush();

    virtual bool getOutFormat(RawMediaFormat& fmt);


protected:
    void fireFrame(AVFramePtr frame);

protected:
    MediaDecoderSink*	m_sink;
    AVCodecContext*		m_codecCtx;

};




} /* namespace av */

#endif /* CAUDIODECODER_H_ */
