/*
 * MediaHelper.cpp
 *
 *  Created on: 2016年4月15日
 *      Author: terry
 */

#include <MediaHelper.h>

namespace av
{

MediaHelper::MediaHelper()
{
}

MediaHelper::~MediaHelper()
{
}

bool MediaHelper::isVideo(AVFramePtr& frame)
{
	return (frame->nb_samples <= 0);
}

bool MediaHelper::isVideo(AVFrame* frame)
{
	return (frame->nb_samples <= 0);
}

bool MediaHelper::isAudio(AVFramePtr& frame)
{
	return (frame->nb_samples > 0);
}

bool MediaHelper::isAudio(AVFrame* frame)
{
	return (frame->nb_samples > 0);
}

bool MediaHelper::isVideo(AVPacket& pkt)
{
    return (pkt.stream_index == MEDIA_TYPE_VIDEO);
}

bool MediaHelper::isVideo(AVPacket* pkt)
{
    return (pkt->stream_index == MEDIA_TYPE_VIDEO);
}



} /* namespace av */
