/*    file: TSFileMediaSink.h
 *    desc:
 *   
 * created: 2016-07-05
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined TSFILEMEDIASINK_H_
#define TSFILEMEDIASINK_H_

#include "FileMediaSink.h"
#include "TCriticalSection.h"
#include "Ffmpeg.h"
#include "FfmpegUtil.h"
#include "TByteBuffer.h"
#include "AudioTranscoder.h"
#include "TThread.h"
#include "PacketQueue.h"
#include "TimePoint.h"



namespace av
{

class TSFileMediaSink: public FileMediaSink, comn::Thread
{
public:
    TSFileMediaSink();
    virtual ~TSFileMediaSink();

    virtual bool open(const char* filename);
    virtual void close();
    virtual bool isOpen();

    virtual void onMediaFormat(const MediaFormat& fmt);

    virtual void onMediaPacket(AVPacketPtr& pkt);

    virtual void onMediaEvent(int event);

    virtual int setFile(const char* filename);

    virtual const char* getFile();

protected:
    virtual int run();
    virtual void doStop();

    void handlePacket(AVPacketPtr& pkt);

protected:
    bool openFile(const char* filename, const MediaFormat& fmt);

    void writeVideo(AVPacketPtr& inPkt);
    void writeAudio(AVPacketPtr& inPkt);
    void doWriteAudio(AVPacket* pkt);

protected:
    bool openFile();
    void closeFile();
    bool isFileOpen();

    bool isG711(const MediaFormat& fmt);
    bool isMp4(AVFormatContext* fmtCtx);

protected:
    comn::CriticalSection   m_cs;

    MediaFormat m_fmt;

    std::string m_fileName;

    int	m_maxDuration;	/// milliseconds

    int m_videoIndex;
    int m_audioIndex;

    int64_t m_videoStartTime;
    int64_t m_audioStartTime;

    AVFormatContext*	m_fmtCtx;

    bool    m_changeFile;
    AudioTranscoder	m_audioTranscoder;
    bool    m_doAudioTranscode;
    comn::ByteBuffer    m_audioBuffer;

    PacketQueue m_pktQueue;

    //TimePoint	m_audioInPoint;
    //TimePoint	m_audioPoint;

};


} /* namespace av */



#endif //TSFILEMEDIASINK_H_

