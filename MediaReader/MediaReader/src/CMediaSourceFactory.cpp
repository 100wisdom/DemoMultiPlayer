/*
 * CMediaSourceFactory.cpp
 *
 *  Created on: 2016年3月18日
 *      Author: terry
 */

#include "CMediaSourceFactory.h"
#include "CMediaSource.h"
#include "TStringUtil.h"
#include "MtcpMediaSource.h"


namespace av
{

CMediaSourceFactory::CMediaSourceFactory()
{
}

CMediaSourceFactory::~CMediaSourceFactory()
{
}

MediaSource* CMediaSourceFactory::create(const std::string& url, const std::string& params)
{
    if (comn::StringUtil::startsWith(url, "mtcp://"))
    {
        return new MtcpMediaSource();
    }
    
	CMediaSource* source = new CMediaSource();
	return source;
}

void CMediaSourceFactory::destroy(MediaSource* source)
{
	delete source;
}



} /* namespace av */
