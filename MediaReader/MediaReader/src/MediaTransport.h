/*
 * MediaTransport.h
 *
 *  Created on: 2016年6月21日
 *      Author: terry
 */

#ifndef MEDIATRANSPORT_H_
#define MEDIATRANSPORT_H_

#include "BasicType.h"
#include <string.h>


namespace av
{



enum MediaTransportTag
{
	kMediaCommand = 1,	/// 载体为 json
	kMediaData			/// 载体为媒体数据
};

enum MediaTransportCommand
{
	kMediaCmdConnect,
	kMediaCmdResp,
    kMediaCmdError,

	kMediaCmdPlay,
	kMediaCmdPause,
	kMediaCmdStop,

};


struct MediaTransportHeader
{
	int16_t	magic;	/// 魔术字
	char	tag;	/// 主类型 @see MediaTransportTag
	char	subtype;	/// 子类型, tag 为command时, 取值为 MediaTransportCommand

	int64_t timestamp;	/// 时间戳
	int32_t	duration;	/// 时长
    int32_t flags;      /// 标记 

	int32_t	length;		/// 载体长度
};


class MediaTransport
{
public:
    static const int16_t	MAGIC = 0x63;

    static void setCommand(MediaTransportHeader& header, char subtype)
    {
        memset(&header, 0, sizeof(header));
        header.magic = MAGIC;
        header.tag = kMediaCommand;
        header.subtype = subtype;
    }

public:
	MediaTransport();
	virtual ~MediaTransport();
};



} /* namespace av */

#endif /* MEDIATRANSPORT_H_ */
