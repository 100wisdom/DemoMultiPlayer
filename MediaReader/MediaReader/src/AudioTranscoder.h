/*
 * AudioTranscoder.h
 *
 *  Created on: 2016年5月19日
 *      Author: terry
 */

#ifndef AUDIOTRANSCODER_H_
#define AUDIOTRANSCODER_H_

#include "MediaFormat.h"
#include "FfmpegUtil.h"
#include <string>
#include "MediaStream.h"


namespace av
{

class AudioTranscoder
{
public:
	AudioTranscoder();
	virtual ~AudioTranscoder();

	bool open(const MediaFormat& fmt, AVCodecID codecID, int channels, int sampleRate);
	void close();
	bool isOpen();

	bool getMediaFormat(MediaFormat& fmt);

	bool transcode(AVPacket* inPkt, AVPacket* outPkt);

	AVCodecContext* getCodecContext();

protected:
	int openEncoder(AVCodecID codec, int channels, int sampleRate);
	void closeEncoder();
	bool isEncoderOpen();

	bool isSamleFormat(int fmt, int channels, int sampleRate);

	bool getAudioConfig(std::string& config);

	void convertAndStore(AVFramePtr& frame);

	int resample(AVFrame* inFrame, AVFrame* outFrame);

	void storeFrame(AVFramePtr& frame);
	bool loadFrame(AVFrame* frame, int nb_samples);
	void closeFifo();
	int getFifoSize();

	int doEncode(AVPacket* pkt);

	int openDecoder();
	void closeDecoder();

protected:
	MediaFormat	m_fmt;
	AVCodecID m_codecID;
	int m_channels;
	int m_sampleRate;

	AVCodecContext*	m_decContext;


	AVCodecContext* m_context;
	SwrContext*		m_swrContext;
	AVAudioFifo*	m_fifo;
	int64_t         m_sampleCount;
	std::string     m_extradata;


};


} /* namespace av */

#endif /* AUDIOTRANSCODER_H_ */
