/*
 * AVFilterPtr.h
 *
 *  Created on: 2016年1月4日
 *      Author: terry
 */

#ifndef SRC_AVFILTERPTR_H_
#define SRC_AVFILTERPTR_H_

#include "MediaStream.h"

extern "C"
{
	#include "libavfilter/avfilter.h"
	#include "libavfilter/buffersink.h"
	#include "libavfilter/buffersrc.h"
	#include "libavutil/opt.h"
}


namespace av
{


struct AVFilterGraphDeleter
{
	void operator ()(AVFilterGraph* pGraph)
	{
		avfilter_graph_free(&pGraph);
	}
};

struct AVFilterDeleter
{
	void operator ()(AVFilterContext* pFilter)
	{
		avfilter_free(pFilter);
	}
};

struct AVFilterInOutDeleter
{
	void operator () (AVFilterInOut* pInOut)
	{
		avfilter_inout_free(&pInOut);
	}
};


typedef std::shared_ptr< AVFilterGraph >		AVFilterGraphptr;
typedef std::shared_ptr< AVFilterContext >		AVFilterContextPtr;
typedef std::shared_ptr< AVFilterInOut >		AVFilterInOutptr;


}


#endif /* SRC_AVFILTERPTR_H_ */
