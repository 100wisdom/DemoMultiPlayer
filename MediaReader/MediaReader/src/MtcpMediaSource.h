/*
 * MtcpMediaSource.h
 *
 *  Created on: 2016年6月23日
 *      Author: terry
 */

#ifndef MTCPMEDIASOURCE_H_
#define MTCPMEDIASOURCE_H_

#include "MediaSource.h"
#include "TCriticalSection.h"
#include "Socket.h"
#include "SocketPair.h"
#include "MediaTransport.h"
#include "TByteBuffer.h"
#include "ParamSet.h"

#include "TimePoint.h"
//#include "TimestampChecker.h"


namespace av
{

class MtcpMediaSource : public MediaSource
{
public:
	MtcpMediaSource();
	virtual ~MtcpMediaSource();

    virtual int open(const std::string& url, const std::string& params);

    virtual void close();

    virtual bool isOpen();

    virtual bool getFormat(MediaFormat& fmt);

    virtual int getDuration();

    virtual int play();
    virtual int pause();
    virtual void stop();

    virtual int getState();

    virtual bool seekable();

    virtual int seek(int64_t offset);

    virtual int64_t getTime();

    virtual int setScale(float scale);

    virtual float getScale();

    virtual int read(AVPacket& pkt);

    virtual void interrupt();

    virtual bool isLive();

protected:
    bool checkReadable(long millsec);
    bool checkConnected(long millsec);

    bool doPlay(const std::string& name);

    bool doReadFrame();

protected:
    std::string	m_url;
    std::string	m_params;
    MediaFormat	m_format;
    StreamState	m_state;
    comn::CriticalSection	m_cs;
    int64_t	m_curTime;
    
    comn::Socket    m_socket;
    util::SocketPair    m_sockPair;

    comn::ByteBuffer m_buffer;
    util::ParamSet    m_paramSet;
    size_t  m_timeout;

    //util::TimestampChecker m_audioChecker;

};


} /* namespace av */

#endif /* MTCPMEDIASOURCE_H_ */
