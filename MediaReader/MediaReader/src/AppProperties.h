/*
 * AppProperties.h
 *
 *  Created on: 2015年12月22日
 *      Author: terry
 */

#ifndef APPPROPERTIES_H_
#define APPPROPERTIES_H_

#include "IniProperties.h"


class AppProperties
{
public:
	AppProperties();
	virtual ~AppProperties();

    static comn::IniProperties& getProperties();

	static void setPath(const char* filename);

	static std::string getPath();


};


#endif /* APPPROPERTIES_H_ */
