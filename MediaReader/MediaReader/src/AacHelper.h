﻿/*
 * AacHelper.h
 *
 *  Created on: 2015年12月7日
 *      Author: terry
 */

#ifndef AACHELPER_H_
#define AACHELPER_H_

#include "BasicType.h"
#include <string>

namespace av
{

class AacHelper
{
public:
	static const size_t ADTS_HEADER_SIZE = 7;

public:
	AacHelper();
	virtual ~AacHelper();

	/// 二进制
	static bool makeConfig(int profile, int freq, int channels, std::string& config);
	
	/// 字符串形式
	static std::string makeConfig(int profile, int freq, int channels);

	static bool makeAdtsHeader(int profile, int freq, int channels, int packetLen, uint8_t* buf, size_t size);

	static int getFreqIndex(int freq);

	static bool isAdts(uint8_t* buf, size_t size);

	static bool parseConfig(const uint8_t* buf, size_t size, int& profile, int& freq, int& channels);

	static bool parseAdts(uint8_t* buf, size_t size, int& profile, int& freq, int& channels);

};


} /* namespace av */

#endif /* AACHELPER_H_ */
