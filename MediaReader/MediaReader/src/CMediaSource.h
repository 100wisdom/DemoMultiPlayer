/*
 * CMediaSource.h
 *
 *  Created on: 2016年3月18日
 *      Author: terry
 */

#ifndef CMEDIASOURCE_H_
#define CMEDIASOURCE_H_

#include "MediaSource.h"
#include "TCriticalSection.h"


namespace av
{

class CMediaSource: public MediaSource
{
public:
	CMediaSource();
	virtual ~CMediaSource();

	virtual int open(const std::string& url, const std::string& params);

	virtual void close();

	virtual bool isOpen();

	virtual bool getFormat(MediaFormat& fmt);

    virtual int getDuration();

	virtual int play();
	virtual int pause();
	virtual void stop();

	virtual int getState();

	virtual bool seekable();

	virtual int seek(int64_t offset);

	virtual int64_t getTime();

	virtual int setScale(float scale);

	virtual float getScale();

	virtual int read(AVPacket& pkt);
    
    virtual void interrupt();

    virtual bool isLive();


    int checkInterrupt();

protected:
    int openInput(const std::string& url, const std::string& params);
    void closeInput();
    bool isInputOpen();

    void fetchMediaFormat();

    void setState(StreamState state);

    int parseParams(AVDictionary **pm, const std::string& params);

    int doRead(AVPacket& pkt);

    bool isFile();

protected:
	std::string	m_url;
	std::string	m_params;
	AVDictionary* m_dict;
	AVFormatContext* m_fmtContext;
	int m_videoIdx;
	int m_audioIdx;
	MediaFormat	m_format;
	StreamState	m_state;
	comn::CriticalSection	m_cs;
    int m_interrupted;
    bool 	m_paused;
    bool  	m_isFile;
    int64_t	m_curTime;

};


} /* namespace av */

#endif /* CMEDIASOURCE_H_ */
