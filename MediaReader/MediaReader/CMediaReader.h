/*
 * CMediaReader.h
 *
 *  Created on: 2016��8��13��
 *      Author: zhengboyuan
 */

#ifndef CMEDIAREADER_H_
#define CMEDIAREADER_H_

#include "MediaReader.h"
#include "MediaSourceFactory.h"
#include "SharedPtr.h"
#include "FileMediaSink.h"
#include "MediaObject.h"
#include "TCriticalSection.h"
#include "TThread.h"

namespace av
{

class CMediaReader: public MediaReader
{
public:
	CMediaReader();
	virtual ~CMediaReader();

	virtual int open(const std::string& url, const std::string& params);

	virtual void close();

	virtual bool isOpen();

	virtual int getFormat(MediaFormat& fmt);

	virtual int getDuration();

	virtual int play();
	virtual int pause();
	virtual void stop();

	virtual int getState();

	virtual bool seekable();

	virtual int seek(int64_t offset);

	virtual int64_t getTime();

	virtual int setScale(float scale);

	virtual float getScale();

	virtual int read(AVPacketPtr& pkt);

	virtual void interrupt();

	virtual bool isLive();


	virtual int  startRecord(const std::string& filename);
	virtual void stopRecord();
	virtual bool isRecording();

	virtual int startRender();
	virtual void stopRender();
	virtual bool isRendering();
	virtual void setVideoWindow(void* window);

	virtual void setEventCallback(ReaderEventCallback cb, void* context);

protected:
	void fireMediaEvent(int type, int64_t value);
	void fireMediaFormat(const MediaFormat& fmt);

protected:

	MediaSourcePtr	m_source;
	MediaSourceFactoryPtr	m_factory;
	FileMediaSinkPtr	m_fileSink;

	MediaFormat	m_format;

	comn::CriticalSection	m_cs;
	bool	m_endOfStream;

	ReaderEventCallback	m_eventCallback;
	void*	m_eventContext;

};


} /* namespace av */

#endif /* CMEDIAREADER_H_ */
