/*
 * TimeShift.h
 *
 *  Created on: 2016年5月19日
 *      Author: terry
 */

#ifndef UTIL_TIMESHIFT_H_
#define UTIL_TIMESHIFT_H_

#include "BasicType.h"


namespace util
{

class TimeShift
{
public:
	TimeShift():
		m_start()
	{

	}

	void reset()
	{
		m_start = 0;
	}

	int64_t shift(int64_t ts)
	{
		if (m_start == 0)
		{
			m_start = ts;
		}

		return ts - m_start;
	}

protected:
	int64_t	m_start;

};


}


#endif /* UTIL_TIMESHIFT_H_ */
