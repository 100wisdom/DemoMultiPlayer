/*
 * H265PropParser.cpp
 *
 *  Created on: 2016年4月7日
 *      Author: terry
 */

#include "H265PropParser.h"

namespace av
{

const uint8_t H265PropParser::s_startCode[START_CODE_LENGTH] = { 0, 0, 0, 1};


H265PropParser::H265PropParser()
{
}

H265PropParser::~H265PropParser()
{
}

const uint8_t* H265PropParser::getStartCode()
{
    return s_startCode;

}

bool H265PropParser::startWithH264Code(const uint8_t* data, size_t length)
{
    if (length < 4)
    {
        return false;
    }

    return (data[0] == 0 && data[1] == 0 && data[2] == 0 && data[3] == 1);
}

size_t H265PropParser::findH264StartCode(const uint8_t* data, size_t length, size_t start)
{
    for (size_t i = start; i < length - 4; ++ i)
    {
        if (data[i] == 0)
        {
            if (startWithH264Code(&data[i], length - i))
            {
                return i;
            }
        }
    }
    return -1;
}

bool H265PropParser::splitPropSet(const uint8_t* data, size_t length,
				std::string& vps, std::string& sps, std::string& pps)
{
	size_t start = START_CODE_LENGTH;
	size_t nextPos = findH264StartCode(data, length, start);
	if (nextPos == size_t(-1))
	{
		return false;
	}

	vps = std::string((const char*)(data+start), nextPos - start);

	start = nextPos + START_CODE_LENGTH;
	nextPos = findH264StartCode(data, length, start);
	if (nextPos == size_t(-1))
	{
		return false;
	}
	sps = std::string((const char*)(data+start), nextPos - start);

	start = nextPos + START_CODE_LENGTH;
	nextPos = findH264StartCode(data, length, start);
	if (nextPos == size_t(-1))
	{
		nextPos = length;
	}
	pps = std::string((const char*)(data+start), nextPos - start);

	return true;
}




} /* namespace av */
