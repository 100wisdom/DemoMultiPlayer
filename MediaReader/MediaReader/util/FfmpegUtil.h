/*
 * FfmpegUtil.h
 *
 *  Created on: 2016��3��14��
 *      Author: terry
 */

#ifndef FFMPEGUTIL_H_
#define FFMPEGUTIL_H_


#ifdef _MSC_VER
#pragma warning(disable: 4244)
#endif //WIN32


#ifdef __cplusplus
#define __STDC_CONSTANT_MACROS
#ifdef _STDINT_H
#undef _STDINT_H
#endif
# include <stdint.h>
#endif

#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif


extern "C"
{
	#include <libavutil/avutil.h>
	#include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
	#include <libavfilter/avfilter.h>
	#include <libswresample/swresample.h>
	#include <libswscale/swscale.h>

	#include <libavutil/time.h>
	#include <libavutil/audio_fifo.h>
}



namespace ffmpeg
{


class Util
{
public:
	Util();
	virtual ~Util();
};


struct CodecContextDeleter
{
	void operator () (AVCodecContext* context);
};

struct FormatContextDeleter
{
	void operator () (AVFormatContext* context);
};

struct AVDictionaryDeleter
{
	void operator () (AVDictionary* dict);
};

struct AVAudioFifoDeleter
{
	void operator () (AVAudioFifo* fifo);
};

struct SwrContextDeleter
{
	void operator () (SwrContext* context);
};

struct AVFrameDeleter
{
	void operator () (AVFrame* frame);
};

struct PacketDeleter
{
	void operator () (AVPacket* pkt);
};

struct AVIODeleter
{
	void operator () (AVIOContext* context);
};



bool av_dict_get_int(AVDictionary* m, const char* key, int& value);
bool av_dict_get_int(AVDictionary* m, const char* key, unsigned int& value);
bool av_dict_get_int(AVDictionary* m, const char* key, int64_t& value);


}





/// deleter
/// get name
///




#endif /* FFMPEGUTIL_H_ */
