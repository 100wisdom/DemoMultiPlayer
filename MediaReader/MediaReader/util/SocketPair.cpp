/*
 * SocketPair.cpp
 *
 *  Created on: 2012-1-10
 *      Author: terry
 */

#include "SocketPair.h"

namespace util
{


SocketPair::SocketPair() :
        m_serverSocket(),
        m_clientSocket()
{

}

SocketPair::~SocketPair()
{
    close();
}


bool SocketPair::open()
{
	comn::Socket listenSocket;
    if (!listenSocket.open(SOCK_STREAM))
    {
        return false;
    }

    comn::SockAddr addr("127.0.0.1", 0);
    listenSocket.bind(addr);
    listenSocket.listen();

    bool done = makeConnection(listenSocket);

    listenSocket.close();
    return done;
}

void SocketPair::close()
{
    m_clientSocket.close();
    m_serverSocket.close();
}

socket_t SocketPair::getPeerHandle() const
{
    return m_serverSocket.getHandle();
}

socket_t SocketPair::getHandle() const
{
    return m_clientSocket.getHandle();
}

void SocketPair::makeReadable()
{
    int byte = 0x99;
    m_serverSocket.send((char*)&byte, sizeof(int), 0);
}

void SocketPair::clearReadable()
{
    const int BUFFER_MAX = 100;
    char buffer[BUFFER_MAX] = {0};
    int ret = m_clientSocket.receive(buffer, sizeof(buffer));
    while (ret == BUFFER_MAX)
    {
        ret = m_clientSocket.receive(buffer, sizeof(buffer));
    }
}

bool SocketPair::makeConnection(comn::Socket& listenSocket)
{
    if (!m_clientSocket.open(SOCK_STREAM))
    {
        return false;
    }

    bool done = false;
    m_clientSocket.setNonblock(true);
    comn::SockAddr addr = listenSocket.getSockName();
    m_clientSocket.connect(addr);

    if (listenSocket.checkReadable(200))
    {
        m_serverSocket = listenSocket.accept(addr);
        done = m_serverSocket.isOpen();
    }
    else
    {
        m_clientSocket.close();
    }

    return done;
}


}
