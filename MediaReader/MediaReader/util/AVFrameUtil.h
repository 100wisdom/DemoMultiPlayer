/*    file: AVFrameUtil.h
 *    desc:
 *   
 * created: 2016-06-12
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined AVFRAMEUTIL_H_
#define AVFRAMEUTIL_H_

#include "Ffmpeg.h"

class AVFrameUtil
{
public:
    static bool saveAsJpeg(AVFrame *pFrame, const char* filename);

    static bool saveAsBmp(AVFrame *pFrame, const char* filename);

    static bool saveFrame(AVFrame *pFrame, const char* filename);

};


#endif //AVFRAMEUTIL_H_

