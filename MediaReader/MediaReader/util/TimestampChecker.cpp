/*    file: TimestampChecker.cpp
 *    desc:
 * 
 * created: 2016-06-30
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "TimestampChecker.h"

namespace util
{


TimestampChecker::TimestampChecker():
    m_start(),
    m_cur(),
    m_duration(),
    m_count()
{

}

void TimestampChecker::reset()
{
    m_start.reset();
    m_cur.reset();
    m_duration = 0;
    m_count = 0;
}

void TimestampChecker::input(int64_t clk, int64_t pts, int64_t duration)
{
    if (!m_cur.isSet())
    {
        m_start.reset(clk, pts);
        m_cur = m_start;
        m_duration = duration;
        m_count = 1;
    }
    else
    {
        m_cur.reset(clk, pts);
        m_duration += duration;
        m_count ++;
    }
}

int64_t TimestampChecker::getClockDuration() const
{
    return m_cur.m_clock - m_start.m_clock;
}

int64_t TimestampChecker::getPtsDuration() const
{
    return m_cur.m_pts - m_start.m_pts;
}

int64_t TimestampChecker::getDuration() const
{
    return m_duration;
}

int64_t TimestampChecker::getCount() const
{
    return m_count;
}


}