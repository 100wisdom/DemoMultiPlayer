/*
 * SocketPair.h
 *
 *  Created on: 2012-1-10
 *      Author: terry
 */

#ifndef SOCKETPAIR_H_
#define SOCKETPAIR_H_

#include "Socket.h"

namespace util
{

class SocketPair
{
public:
    SocketPair();
    ~SocketPair();

    bool open();
    void close();

    void makeReadable();
    void clearReadable();

    socket_t getHandle() const;
    socket_t getPeerHandle() const;

protected:
    bool makeConnection(comn::Socket& listenSocket);

private:
    comn::Socket      m_serverSocket;
    comn::Socket      m_clientSocket;


};

}

#endif /* SOCKETPAIR_H_ */
