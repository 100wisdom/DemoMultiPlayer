/*
 * Uri.h
 *
 *  Created on: 2013年11月16日
 *      Author: zhengchuanjiang
 */

#ifndef URI_H_
#define URI_H_

#include "BasicType.h"
#include <string>


namespace util
{

typedef unsigned short uint16_t;

class Uri
{
public:
    Uri();
    ~Uri();

    explicit Uri(const std::string& uri);

    std::string m_scheme;
    std::string m_userName;
    std::string m_password;
    std::string m_host;
    uint16_t    m_port;
    std::string m_path;     /// 绝对路径，总是以'/'开头
    std::string m_query;    /// 不包含'?'
    std::string m_fragment; /// 不包含'#'

    bool parse(const std::string& uri);

    std::string toString() const;

    bool isValid() const;

    bool isDefaultPort() const;

    bool isFile() const;

    void clear();

    std::string getUserInfo() const;

    std::string getAuthority() const;

    bool isRelative() const;

    std::string getPathEtc() const;

    std::string getUrlWithoutUser() const;

};





} /* namespace net */
#endif /* URI_H_ */
