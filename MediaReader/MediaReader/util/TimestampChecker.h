/*    file: TimestampChecker.h
 *    desc:
 *   
 * created: 2016-06-30
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined TIMESTAMPCHECKER_H_
#define TIMESTAMPCHECKER_H_

#include "TimePoint.h"
#include "TimeHelper.h"


namespace util
{

class TimestampChecker
{
public:
    
    TimestampChecker();

    void reset();
    
    void input(int64_t clk, int64_t pts, int64_t duration);

    int64_t getClockDuration() const;

    int64_t getPtsDuration() const;

    int64_t getDuration() const;

    int64_t getCount() const;


    av::TimePoint m_start;
    av::TimePoint m_cur;
    int64_t   m_duration;
    int64_t   m_count;

};


}



#endif //TIMESTAMPCHECKER_H_

