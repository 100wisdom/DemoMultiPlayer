/*
 * PacketQueue.h
 *
 *  Created on: 2016年3月17日
 *      Author: terry
 */

#ifndef PACKETQUEUE_H_
#define PACKETQUEUE_H_

#include "MediaStream.h"
#include <deque>
#include "TCriticalSection.h"
#include "TEvent.h"


namespace av
{

class PacketQueue
{
public:
	PacketQueue();
	virtual ~PacketQueue();

	size_t size();

	bool empty();

	size_t push(AVPacketPtr& packet);

	AVPacketPtr pop();

	bool pop(AVPacketPtr& packet);

	void clear();

	bool timedwait(int ms);

	bool pop(AVPacketPtr& packet, int ms);

	bool popSize(AVPacketPtr& packet, size_t& count);
	size_t popTimeout(AVPacketPtr& packet, int ms);

	void cancelWait();

	size_t dropUntilKeyFrame();

	size_t reserveFront(size_t count);

protected:
	typedef std::deque< AVPacketPtr >  PacketDeque;

	bool isVideoKey(AVPacketPtr& pkt);

protected:
	PacketDeque m_packets;
	comn::CriticalSection   m_cs;
	comn::Event	m_event;

};


} /* namespace av */

#endif /* PACKETQUEUE_H_ */
