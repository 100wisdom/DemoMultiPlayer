/*
 * ParamSet.h
 *
 *  Created on: 2015年12月20日
 *      Author: terry
 */

#ifndef PARAMSET_H_
#define PARAMSET_H_

#include <string>
#include <sstream>
#include <cstring>
#include <map>

#include "TCriticalSection.h"


namespace util
{

class ParamSet
{
public:
	static const char	SEPERATOR = '=';
	static const char	SEG = ';';

	static const char	SPLITTER = '.';

public:
	ParamSet();
	virtual ~ParamSet();

	void set(const char* key, const char* value);

	void set(const std::string& key, const std::string& value);

	std::string toString();

	std::string toString(char sep, char seg);

	bool fromString(const std::string& str, char sep, char seg);

	bool parse(const std::string& str, char sep, char seg);

	bool get(const std::string& key, std::string& value);

	size_t size();

	bool getAt(size_t idx, std::string& key, std::string& value);

	void clear();

	bool exists(const std::string& key);

	size_t filter(const std::string& sec, ParamSet& dest);


	template < class T >
	void set(const std::string& key, const T& value)
	{
		std::ostringstream ss;
		ss << value;
		set(key, ss.str());
	}


	template < class T >
	bool get(const std::string& key, T& value)
	{
		std::string str;
		if (!get(key, str))
		{
			return false;
		}

		std::istringstream iss(str);
		iss >> value;
		return true;
	}


protected:
	typedef std::map< std::string, std::string >	StringMap;


protected:
	StringMap	m_map;
	comn::CriticalSection	m_cs;


};



} /* namespace av */

#endif /* PARAMSET_H_ */
