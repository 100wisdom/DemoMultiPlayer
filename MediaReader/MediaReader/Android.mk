LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := MediaReader

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../comn/include \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/util \
	$(LOCAL_PATH)/src \
	$(LOCAL_PATH)/../third_party/ffmpeg/include \
	$(LOCAL_PATH)/../third_party/jsoncpp-0.10.6/include \
	$(LOCAL_PATH)/jni \
	
	
	
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

LOCAL_CPPFLAGS += -fexceptions 

SRC_FILES := $(wildcard $(LOCAL_PATH)/*.cpp $(LOCAL_PATH)/util/*.cpp $(LOCAL_PATH)/core/*.cpp $(LOCAL_PATH)/src/*.cpp $(LOCAL_PATH)/jni/*.cpp)
SRC_FILES := $(SRC_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES += $(SRC_FILES)

LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog -landroid -latomic

LOCAL_STATIC_LIBRARIES := \
	comn \
	avcodec avformat swscale avutil avfilter swresample \
	jsoncpp
	
LOCAL_SHARED_LIBRARIES := \
	
	

include $(BUILD_SHARED_LIBRARY)

