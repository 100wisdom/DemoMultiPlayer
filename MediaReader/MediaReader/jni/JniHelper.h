/*    file: JniHelper.h
 *    desc:
 *   
 * created: 2016-04-11
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined JNIHELPER_H_
#define JNIHELPER_H_

#include <jni.h>
#include <string>


namespace jni
{


class Object
{
public:
    Object(JNIEnv *env, const char* className, jobject obj);

    bool isValid() const;

    jfieldID getFieldID(const char* name, const char* sig);
    
    bool getInt(const char* name, jint& value);

    bool getBool(const char* name, jboolean& value);

    bool getByte(const char* name, jbyte& value);

    bool getChar(const char* name, jchar& value);
    
    bool getShort(const char* name, jshort& value);

    bool getLong(const char* name, jlong& value);

    bool getFloat(const char* name, jfloat& value);

    bool getDouble(const char* name, jdouble& value);

    bool getObject(const char* name, const char* sig, jobject& obj);
    
    bool getByteArray(const char* name, jbyteArray& arr);


    bool setInt(const char* name, jint value);
    bool setBool(const char* name, jboolean value);
    bool setByte(const char* name, jbyte value);
    bool setChar(const char* name, jchar value);
    bool setShort(const char* name, jshort value);
    bool setLong(const char* name, jlong value);
    bool setFloat(const char* name, jfloat value);
    bool setDouble(const char* name, jdouble value);

	bool setObject(const char* name, const char* sig, jobject value);
	bool setByteArray(const char* name, jbyteArray& arr);

protected:
    JNIEnv* m_env;
    jobject m_obj;
    jclass  m_clazz;
    std::string m_className;

};


class ByteArray
{
public:
    ByteArray(JNIEnv *env, jbyteArray arr);
    ~ByteArray();

    jsize length();

    jbyte* element();

    void release();

protected:
    JNIEnv* m_env;
    jbyteArray m_arr;
    jbyte* m_element;

private:
    ByteArray(const ByteArray& arr);


};


class UtfString
{
public:
    UtfString(JNIEnv *env, jstring str);
    ~UtfString();
    
    const char* c_str();
    jsize length();

    void release();
    
protected:
    JNIEnv* m_env;
    jstring m_str;
    const char* m_element;

private:
    UtfString(const UtfString& str);

};


class JniHelper
{
public:

	static jstring toString(JNIEnv* env, const char* sz);

	static void setAt(JNIEnv* env, jintArray arr, int idx, int value);

	static void setAt(JNIEnv* env, jlongArray arr, int idx, int64_t value);

};


}




#endif //JNIHELPER_H_

