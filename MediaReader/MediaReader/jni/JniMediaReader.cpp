/*
 * JniMediaReader.cpp
 *
 *  Created on: 2016��8��12��
 *      Author: zhengboyuan
 */


#include <jni.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <android/native_window_jni.h>
#include <android/log.h>

#include "JniMediaReader.h"
#include "LibMediaReader.h"
#include "MediaReader.h"

#include "SharedPtr.h"
#include "TCriticalSection.h"

#include "JniHelper.h"

#include "Ffmpeg.h"
#include "FfmpegUtil.h"
#include "H264PropParser.h"
#include "H265PropParser.h"
#include "TDiscreteArray.h"


JavaVM* g_Jvm = NULL;

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
	__android_log_print(ANDROID_LOG_INFO, "jni", "--- JNI_OnLoad. JniMediaReader vm:%p", vm);

	g_Jvm = vm;

	return JNI_VERSION_1_4;
}

void JNI_OnUnload(JavaVM *vm, void *reserved)
{

}


typedef std::shared_ptr< av::MediaReader >		MediaReaderPtr;
MediaReaderPtr getReader(int handle);


class ReaderExtra
{
public:
	ReaderExtra():
		m_handle(),
		m_listener()
	{
	}

	int	m_handle;
	jobject m_listener;
	comn::CriticalSection m_cs;

	void close(JNIEnv * env)
	{
		comn::AutoCritSec lock(m_cs);
		if (m_listener)
		{
			env->DeleteGlobalRef(m_listener);
			m_listener = NULL;
		}
	}


};

typedef std::shared_ptr< ReaderExtra >	ReaderExtraPtr;
typedef comn::DiscreteArray< ReaderExtraPtr, MAX_READER >	ReaderExtraArray;

ReaderExtraArray s_readerExtras;



static void fireEvent(JNIEnv* env, jobject jlistener, int handle, int event, int64_t value)
{
	jclass cls = env->GetObjectClass(jlistener);
	//CLog::info("begin jni event sink. cls:%p", cls);

	jmethodID callback = env->GetMethodID(cls, "onReaderEvent", "(IIJ)V");
	//CLog::info("begin jni event sink. callback:%p", callback);
	if (callback)
	{
		env->CallVoidMethod(jlistener, callback, handle, event, value);
	}
}

static void JniReaderEventCallback(void* reader, int event, int64_t value, void* context)
{
	__android_log_print(ANDROID_LOG_INFO, "jni", "reader event. event:%d", event);
	
	int handle = reinterpret_cast<int>(context);
	if ((handle < 0) || (handle >= MAX_READER))
	{
		return;
	}

	ReaderExtraPtr extra = s_readerExtras.getAt(handle);
	if (!extra->m_listener)
	{
		return;
	}

	JNIEnv* env = NULL;
	g_Jvm->AttachCurrentThread(&env, NULL);

	{
		comn::AutoCritSec lock(extra->m_cs);
		fireEvent(env, extra->m_listener, extra->m_handle, event, value);
	}

	g_Jvm->DetachCurrentThread();
}





/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_init
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1init
  (JNIEnv * env, jclass thiz)
{
	__android_log_print(ANDROID_LOG_INFO, "jni", "mreader_1init");
	
	int ret = mreader_init();

	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_quit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_wits100_av_JniMediaReader_mreader_1quit
  (JNIEnv * env, jclass thiz)
{
	mreader_quit();
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_open
 * Signature: ([ILjava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1open
  (JNIEnv * env, jclass thiz, jintArray jhandle , jstring jurl, jstring jparams)
{
	jni::UtfString url(env, jurl);
	jni::UtfString params(env, jparams);

	mreader_t handle = -1;
	int ret = mreader_open(&handle, url.c_str(), params.c_str());
	if (ret == 0)
	{
		ReaderExtraPtr extra(new ReaderExtra());
		extra->m_handle = handle;
		s_readerExtras.putAt(handle, extra);

		jni::JniHelper::setAt(env, jhandle, 0, handle);
		
		__android_log_print(ANDROID_LOG_INFO, "jni", "mreader_1open return handle:%d", handle);
	}

	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_close
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1close
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	int ret = mreader_close(jhandle);

	ReaderExtraPtr extra;
	s_readerExtras.remove(jhandle, extra);
	if (extra)
	{
		extra->close(env);
	}

	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_isOpen
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1isOpen
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_isOpen(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_getFormat
 * Signature: (ILcom/wits100/av/MFormat;)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1getFormat
  (JNIEnv * env, jclass thiz, jint jhandle, jobject jfmt)
{
	MediaReaderPtr reader = getReader(jhandle);
	if (!reader)
	{
		return ENODEV;
	}

	int ret = EAGAIN;
	av::MediaFormat format;
	ret = reader->getFormat(format);
	
	__android_log_print(ANDROID_LOG_INFO, "jni", "getFormat video profile:%d", format.m_profile);
	
	if (ret == 0)
	{
		//
		jni::Object obj(env, "com/wits100/av/MFormat", jfmt);
		obj.setInt("width", format.m_width);
		obj.setInt("height", format.m_height);
		obj.setInt("codec", format.m_codec);
		obj.setInt("fps", format.m_framerate);
		obj.setInt("profile", format.m_profile);
		obj.setInt("clockRate", format.m_clockRate);

		obj.setInt("audioCodec", format.m_audioCodec);
		obj.setInt("channels", format.m_channels);
		obj.setInt("sampleRate", format.m_sampleRate);
		obj.setInt("audioProfile", format.m_audioProfile);
		obj.setInt("audioRate", format.m_audioRate);

		jbyteArray props = env->NewByteArray(format.m_videoProp.length());
		env->SetByteArrayRegion(props, 0, format.m_videoProp.length(), (const jbyte*)format.m_videoProp.c_str());
		obj.setByteArray("props", props);

		if (format.m_codec == av::MEDIA_CODEC_H264)
		{
			std::string sps;
			std::string pps;
			av::H264PropParser::splitPropSet(format.m_videoProp.data(), format.m_videoProp.length(), sps, pps);

			jbyteArray jsps = env->NewByteArray(sps.length());
			env->SetByteArrayRegion(jsps, 0, sps.length(), (const jbyte*)sps.c_str());
			obj.setByteArray("sps", jsps);

			jbyteArray jpps = env->NewByteArray(pps.length());
			env->SetByteArrayRegion(jpps, 0, pps.length(), (const jbyte*)pps.c_str());
			obj.setByteArray("pps", jpps);
		}
		else if (format.m_codec == av::MEDIA_CODEC_H265)
		{
			std::string sps;
			std::string pps;
			std::string vps;
			av::H265PropParser::splitPropSet(format.m_videoProp.data(), format.m_videoProp.length(), vps, sps, pps);

			jbyteArray jvps = env->NewByteArray(vps.length());
			env->SetByteArrayRegion(jvps, 0, vps.length(), (const jbyte*)vps.c_str());
			obj.setByteArray("vps", jvps);

			jbyteArray jsps = env->NewByteArray(sps.length());
			env->SetByteArrayRegion(jsps, 0, sps.length(), (const jbyte*)sps.c_str());
			obj.setByteArray("sps", jsps);

			jbyteArray jpps = env->NewByteArray(pps.length());
			env->SetByteArrayRegion(jpps, 0, pps.length(), (const jbyte*)pps.c_str());
			obj.setByteArray("pps", jpps);
		}

		if (format.m_audioConfig.length() > 0)
		{
			jbyteArray audioConfig = env->NewByteArray(format.m_audioConfig.length());
			env->SetByteArrayRegion(audioConfig, 0, format.m_audioConfig.length(), (const jbyte*)format.m_audioConfig.c_str());
			obj.setByteArray("audioConfig", audioConfig);
		}
		
	}

	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_getDuration
 * Signature: (I[I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1getDuration
  (JNIEnv * env, jclass thiz, jint jhandle, jintArray jduration)
{
	int duration = 0;
	int ret = mreader_getDuration(jhandle, &duration);
	jni::JniHelper::setAt(env, jduration, 0, duration);
	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_isLive
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1isLive
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	int ret = mreader_isLive(jhandle);
	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_seekable
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1seekable
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_seekable(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_play
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1play
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_play(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_pause
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1pause
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_pause(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_stop
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1stop
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_stop(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_getState
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1getState
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_getState(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_read
 * Signature: (ILcom/wits100/av/MPacket;)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1read
  (JNIEnv * env, jclass thiz, jint jhandle, jobject jpkt)
{
	//__android_log_print(ANDROID_LOG_INFO, "jni", "mreader_1read: %p", jpkt);

	MediaReaderPtr reader = getReader(jhandle);
	if (!reader)
	{
		return ENODEV;
	}

	av::AVPacketPtr packet(new AVPacket(), ffmpeg::PacketDeleter());
	av_init_packet(packet.get());

	int ret = reader->read(packet);
	if (ret == 0)
	{
		jni::Object obj(env, "com/wits100/av/MPacket", jpkt);

		obj.setInt("type", packet->stream_index + 1);
		obj.setInt("duration", packet->duration);
		obj.setInt("flags", packet->flags);
		obj.setLong("ts", packet->pts);
		
		jbyteArray data = env->NewByteArray(packet->size);
		env->SetByteArrayRegion(data, 0, packet->size, (const jbyte*)packet->data);
		obj.setByteArray("data", data);

		//__android_log_print(ANDROID_LOG_INFO, "jni", "read data: %d", packet->size);
	}

	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_interrupt
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1interrupt
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_interrupt(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_seek
 * Signature: (IJ)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1seek
  (JNIEnv * env, jclass thiz, jint jhandle, jlong offset)
{
	return mreader_seek(jhandle, offset);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_getTime
 * Signature: (I[J)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1getTime
  (JNIEnv * env, jclass thiz, jint jhandle, jlongArray ts)
{
	int64_t offset = 0;
	int ret = mreader_getTime(jhandle, &offset);
	jni::JniHelper::setAt(env, ts, 0, offset);
	return ret;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_startRecord
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1startRecord
  (JNIEnv * env, jclass thiz, jint jhandle, jstring jfilename)
{
	jni::UtfString filename(env, jfilename);
	return mreader_startRecord(jhandle, filename.c_str());
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_stopRecord
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1stopRecord
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_stopRecord(jhandle);
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_isRecording
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1isRecording
  (JNIEnv * env, jclass thiz, jint jhandle)
{
	return mreader_isRecording(jhandle);
}


/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_setEventCallback
 * Signature: (ILcom/wits100/av/JniMediaReaderListener;)I
 */
JNIEXPORT jint JNICALL Java_com_wits100_av_JniMediaReader_mreader_1setEventCallback
  (JNIEnv * env, jclass thiz, jint jhandle, jobject jlistener)
{
	__android_log_print(ANDROID_LOG_INFO, "jni", "setEventCallback. handle:%d", jhandle);
	
	MediaReaderPtr reader = getReader(jhandle);
	if (!reader)
	{
		__android_log_print(ANDROID_LOG_INFO, "jni", "--------- no such reader");
		return ENODEV;
	}

	ReaderExtraPtr extra = s_readerExtras.getAt(jhandle);

	{
		comn::AutoCritSec lock(extra->m_cs);
		extra->m_listener = env->NewGlobalRef(jlistener);
	}

	reader->setEventCallback(JniReaderEventCallback, reinterpret_cast<void*>(jhandle));

	return 0;
}

/*
 * Class:     com_wits100_av_JniMediaReader
 * Method:    mreader_enableLog
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_com_wits100_av_JniMediaReader_mreader_1enableLog
  (JNIEnv * env, jclass, jstring jfilename)
{
	jni::UtfString filename(env, jfilename);
	
	__android_log_print(ANDROID_LOG_INFO, "jni", "mreader_enableLog. file:%s", filename.c_str());
	
	mreader_enableLog(filename.c_str());
}
