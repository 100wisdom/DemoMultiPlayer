package com.wits.demoijkplayer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.wits.media.IjkVideoView;
import com.wits.media.VideoPlayerListener;

import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            IjkMediaPlayer.loadLibrariesOnce(null);
            IjkMediaPlayer.native_profileBegin("libijkplayer.so");

        } catch (Exception e) {
            this.finish();
        }

        setupPlayer();

    }

    @Override
    protected void onStop() {

        stopPlay();

        IjkMediaPlayer.native_profileEnd();

        super.onStop();
    }

    protected void setupPlayer() {
        EditText editText = (EditText)findViewById(R.id.editTextUrl);
        String url = "rtsp://192.168.10.51/";
        editText.setText(url);

        setupVideoView(R.id.surfaceView1);
        setupVideoView(R.id.surfaceView2);
        setupVideoView(R.id.surfaceView3);
    }

    protected void setupVideoView(int id) {
        IjkVideoView videoView = (IjkVideoView)findViewById(id);
        if (videoView != null) {
            setupVideoView(videoView);
        }
    }

    protected void setupVideoView(IjkVideoView videoView) {
        videoView.setListener(new VideoPlayerListener() {
            @Override
            public void onBufferingUpdate(IMediaPlayer mp, int percent) {
            }

            @Override
            public void onCompletion(IMediaPlayer mp) {
            }

            @Override
            public boolean onError(IMediaPlayer mp, int what, int extra) {
                return false;
            }

            @Override
            public boolean onInfo(IMediaPlayer mp, int what, int extra) {
                return false;
            }

            @Override
            public void onPrepared(IMediaPlayer mp) {
                // 视频准备好播放了，但是他不会自动播放，需要手动让他开始。
                mp.start();
            }

            @Override
            public void onSeekComplete(IMediaPlayer mp) {

            }

            @Override
            public void onVideoSizeChanged(IMediaPlayer mp, int width, int height, int sar_num, int sar_den) {
                //在此可以获取到视频的宽和高
            }
        });
    }

    public void onPlayStop(View view) {
        IjkVideoView videoView1 = (IjkVideoView)findViewById(R.id.surfaceView1);
        IjkVideoView videoView2 = (IjkVideoView)findViewById(R.id.surfaceView2);
        IjkVideoView videoView3 = (IjkVideoView)findViewById(R.id.surfaceView3);

        String url = getUrl();
        videoView1.setVideoPath(url);
        videoView1.start();

        videoView2.setVideoPath(url);
        videoView2.start();

        videoView3.setVideoPath(url);
        videoView3.start();
    }

    public String getUrl() {
        EditText editText = (EditText)findViewById(R.id.editTextUrl);
        return editText.getText().toString();
    }

    public void stopPlay() {
        IjkVideoView videoView1 = (IjkVideoView)findViewById(R.id.surfaceView1);
        IjkVideoView videoView2 = (IjkVideoView)findViewById(R.id.surfaceView2);
        IjkVideoView videoView3 = (IjkVideoView)findViewById(R.id.surfaceView3);

        videoView1.stop();
        videoView2.stop();
        videoView3.stop();
    }
}
